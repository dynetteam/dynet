package eu.primes.dynet.internal.variance;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel provides information about the edge colour mapping scheme that has been applied by the EdgeVarianceMapperTask
 * (Multi-network Variation Analysis), focusing on one particular node. It displays a table containing the edges that are
 * adjacent to the currently analyzed node, their attribute values for all the networks included in the mapping, and the
 * calculated variance. 
 * 
 * @author Ivan Hendy Goenawan
 */

public class EdgeInformationPanel extends JPanel{
	
	private DynamicNetwork dynet;
	private List<CyNetwork> networks;
	private String edgeProperty;
	private CyNode analyzedNode;
	private Map<CyEdge, Double> edgeVariationMap;
	private JTable edgeTable;

	public EdgeInformationPanel(DynamicNetwork dynet, final List<CyNetwork> networks, final String edgeProperty, final Map<CyEdge, Double> edgeVariationMap, final CyNode analyzedNode){
		this.dynet = dynet;
		this.networks = networks;
		this.edgeProperty = edgeProperty;
		this.edgeVariationMap = edgeVariationMap;
		this.analyzedNode = analyzedNode;
		
		
		final CyNetwork unionNetwork = dynet.getUnionNetwork();
		final List<CyEdge> adjacentEdges = unionNetwork.getAdjacentEdgeList(analyzedNode, Type.ANY);
		
		edgeTable = new JTable();
		edgeTable.setFillsViewportHeight(true);
		edgeTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		edgeTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		edgeTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		edgeTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		edgeTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyEdge edge = adjacentEdges.get(rowIndex);
				CyNode source = edge.getSource();
				CyNode target = edge.getTarget();
				CyNode neighbor = (source == analyzedNode) ? target : source;
				
				if (columnIndex == 0){
					return unionNetwork.getRow(neighbor).get(CyNetwork.NAME, String.class);
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					return unionNetwork.getRow(edge).get(networkName + "_" + edgeProperty, Object.class);
				}else{
					return edgeVariationMap.get(edge);
				}
			}
			
			@Override
			public int getRowCount() {
				return adjacentEdges.size();
			}
			
			@Override
			public int getColumnCount() {
				return networks.size() + 2;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				Class<?> type = null;
				if (columnIndex == 0){
					type = String.class;
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					CyColumn column = unionNetwork.getDefaultEdgeTable().getColumn(networkName + "_" + edgeProperty);
					if (column != null) type = column.getType();
				}else{
					type = Double.class;
				}
				
				if (type != null){
					return type;
				}else{
					return super.getColumnClass(columnIndex);
				}
			}
		});
		edgeTable.setAutoCreateRowSorter(true);
		edgeTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader tableHeader = edgeTable.getTableHeader();
		tableHeader.getColumnModel().getColumn(0).setHeaderValue("Edge to");
		for (int i = 0; i < networks.size(); i++){
			CyNetwork network = networks.get(i);
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			tableHeader.getColumnModel().getColumn(i + 1).setHeaderValue(networkName);
		}
		tableHeader.getColumnModel().getColumn(networks.size() + 1).setHeaderValue("Variance");
		
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 25, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 40, 20, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel titleLabel = new JLabel("Edge colour currently mapped to variance in \"" + edgeProperty + "\" property:");
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_titleLabel.gridx = 1;
		gbc_titleLabel.gridy = 1;
		add(titleLabel, gbc_titleLabel);
		
		JPanel nodeTablePanel = new JPanel();
		GridBagConstraints gbc_nodeTablePanel = new GridBagConstraints();
		gbc_nodeTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_nodeTablePanel.fill = GridBagConstraints.BOTH;
		gbc_nodeTablePanel.gridx = 1;
		gbc_nodeTablePanel.gridy = 2;
		add(nodeTablePanel, gbc_nodeTablePanel);
		nodeTablePanel.setLayout(new BorderLayout(0, 0));
		nodeTablePanel.add(tableHeader, BorderLayout.NORTH);
		nodeTablePanel.add(edgeTable, BorderLayout.CENTER);
	}
}