package eu.primes.dynet.internal.variance;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.ControlPanel.ControlPanelComponent;
import eu.primes.dynet.internal.ControlPanel.EdgeColourController;
import eu.primes.dynet.internal.ControlPanel.NodeColourController;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This Control Panel component provides the menu for mapping node/edges colour to their variance in a particular attribute
 * or variance in node connectivity across multiple networks (Multi-network variation analysis). Nodes/edges that are more 
 * 'varying' will be given a stronger red colour. The 'connectivity variance' is a new metric that we developed to identify
 * nodes that are most rewired across the different networks. It measures how much a node's edge values (for a chosen
 * attribute) are changing across different networks.
 * 
 * @author Ivan Hendy Goenawan
 */

public class MultiNetworkVariationPanel extends ControlPanelComponent implements NodeColourController, EdgeColourController {
	private ControlPanel controlPanel;
	private DynamicNetwork dynet;
	private CyAppAdapter appAdapter;
	
	private JScrollPane networkListScrollPane;
	private JList<CyNetwork> networkList;
	private JCheckBox highlightNodesCheckBox;
	private JCheckBox highlightEdgesCheckBox;
	private JComboBox<String> edgePropertyComboBox;
	private JComboBox<String> nodePropertyComboBox;
	private JComboBox<String> edgeWeightPropertyComboBox;  //for node connectivity variance
	
	boolean highlightNodesEnabled;
	boolean highlightEdgesEnabled;
	private String edgeProperty;
	private String nodeProperty;
	private String edgeWeightProperty; //for node connectivity variance
	private List<CyNetwork> selectedNetworks;
	
	private Map<CyEdge, Double> edgeVariationMap;
	private Map<CyNode, Double> nodeVariationMap;
	private Map<CyNode, Integer> nodeDegreeMap;
	
	private JSeparator separator;
	private JLabel edgeWeightPropertyLabel;
	private JLabel chooseNetworksLabel;
	private JLabel nodePropertyLabel;
	private JLabel edgePropertyLabel;
	
	public static final String CONNECTIVITY = "DyNet REWIRING";
	private static final String NODE_RESULT_COLUMN_NAME = "DyNet Variance";
	private static final String EDGE_RESULT_COLUMN_NAME = "DyNet Variance";
	
	private static final String NODE_REWIRING_RESULT_COLUMN_NAME = "DyNet Rewiring (Dn-score)";
	private static final String NODE_DEGREE_COLUMN_NAME = "Edge Count";
	private static final String NODE_CORRECTED_RESULT_COLUMN_NAME = "Dn-Score (degree corrected)";
	
	
	
	public MultiNetworkVariationPanel (ControlPanel controlPanel, DynamicNetwork dynet, CyAppAdapter appAdapter, boolean defaultNodesEnabled, boolean defaultEdgesEnabled){
		this.controlPanel = controlPanel;
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		this.highlightNodesEnabled = defaultNodesEnabled;
		this.highlightEdgesEnabled = defaultEdgesEnabled;
		
		
		networkList = new JList<CyNetwork>(){
			@Override
			public boolean getScrollableTracksViewportHeight() {
				return false;
			}
		};
		DefaultListModel<CyNetwork> listModel = new DefaultListModel<CyNetwork>();
		for (CyNetwork network : dynet.getMemberNetworks()){
			listModel.addElement(network);
		}
		networkList.setModel(listModel);
		networkList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		networkList.setSelectionInterval(0, dynet.getMemberNetworks().size() - 1);
		selectedNetworks = dynet.getMemberNetworks();
		networkList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false){
					selectedNetworks = ((JList<CyNetwork>)e.getSource()).getSelectedValuesList();
					update();
				}
			}
		});
		
		//to enable drag-and-drop reordering of the list
		MouseAdapter reorderListener = new MouseAdapter() {
			private int pressIndex = 0;
			private int releaseIndex = 0;
			
			@Override
			public void mousePressed(MouseEvent e) {
				pressIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
			}
	
			@Override
			public void mouseReleased(MouseEvent e) {
				releaseIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
				if (releaseIndex != pressIndex && releaseIndex != -1) {
					DefaultListModel model = (DefaultListModel) ((JList)e.getSource()).getModel();
					Object item = model.elementAt(pressIndex);
					model.removeElementAt(pressIndex);
					model.insertElementAt(item, releaseIndex);
				}
			}
	
			@Override
			public void mouseDragged(MouseEvent e) {
				mouseReleased(e);
				pressIndex = releaseIndex;
			}
		};
		networkList.addMouseListener(reorderListener);
		networkList.addMouseMotionListener(reorderListener);
		
		
		
		highlightNodesCheckBox = new JCheckBox("Highlight most varying nodes");
		highlightNodesCheckBox.setSelected(highlightNodesEnabled);
		if (highlightNodesEnabled){
			controlPanel.takeNodeColourControl(this);
		}
		highlightNodesCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					highlightNodesEnabled = true;
					MultiNetworkVariationPanel.this.controlPanel.takeNodeColourControl(MultiNetworkVariationPanel.this);
					update();
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					highlightNodesEnabled = false;
					
					for(View<CyNode> nodeView : MultiNetworkVariationPanel.this.dynet.getUnionNetworkView().getNodeViews()){
						nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
					}
					MultiNetworkVariationPanel.this.dynet.getUnionNetworkView().updateView();
					
					for (CyNetworkView otherNetworkView : MultiNetworkVariationPanel.this.dynet.getMemberNetworkViews()){
						if (selectedNetworks.contains(otherNetworkView.getModel())){
							for(View<CyNode> nodeView : otherNetworkView.getNodeViews()){
								nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
							}
							otherNetworkView.updateView();
						}
					}
					
					CyTable nodeTable = MultiNetworkVariationPanel.this.dynet.getUnionNetwork().getDefaultNodeTable();
					nodeTable.deleteColumn(NODE_RESULT_COLUMN_NAME);
					nodeTable.deleteColumn(NODE_REWIRING_RESULT_COLUMN_NAME);
					nodeTable.deleteColumn(NODE_DEGREE_COLUMN_NAME);
					nodeTable.deleteColumn(NODE_CORRECTED_RESULT_COLUMN_NAME);
					
					MultiNetworkVariationPanel.this.controlPanel.releaseNodeColourControl(MultiNetworkVariationPanel.this);
				}
				
			}
		});
		
		
		
		List<String> ungroupedNodeAttributes = dynet.getUngroupedNodeAttributes();
		Collections.sort(ungroupedNodeAttributes);
		ArrayList<String> nodeAttributeNameList = new ArrayList<String>();
		nodeAttributeNameList.add(CONNECTIVITY);
		nodeAttributeNameList.addAll(ungroupedNodeAttributes);
		nodeProperty = CONNECTIVITY;
		nodePropertyComboBox = new JComboBox<String>(nodeAttributeNameList.toArray(new String[0]));
		nodePropertyComboBox.setSelectedIndex(0);
		nodePropertyComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange()  == ItemEvent.SELECTED){
					nodeProperty = (String)e.getItem();
					if (nodeProperty.equals(CONNECTIVITY)){
						edgeWeightPropertyLabel.setVisible(true);
						edgeWeightPropertyComboBox.setVisible(true);
					}else{
						edgeWeightPropertyLabel.setVisible(false);
						edgeWeightPropertyComboBox.setVisible(false);
					}
					update();
				}
			}
		});
		
		
		List<String> ungroupedEdgeAttributes = dynet.getUngroupedEdgeAttributes();
		Collections.sort(ungroupedEdgeAttributes);
		edgeWeightProperty = DynamicNetwork.PRESENT;
		edgeWeightPropertyComboBox = new JComboBox<String>(ungroupedEdgeAttributes.toArray(new String[0]));
		edgeWeightPropertyComboBox.setSelectedIndex(ungroupedEdgeAttributes.indexOf(DynamicNetwork.PRESENT));
		edgeWeightPropertyComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					edgeWeightProperty = (String)e.getItem();
					update();
				}
			}
		});
		
		
		highlightEdgesCheckBox = new JCheckBox("Highlight most varying edges");
		highlightEdgesCheckBox.setSelected(highlightEdgesEnabled);
		if (highlightEdgesEnabled){
			controlPanel.takeEdgeColourControl(this);
		}
		highlightEdgesCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					highlightEdgesEnabled = true;
					MultiNetworkVariationPanel.this.controlPanel.takeEdgeColourControl(MultiNetworkVariationPanel.this);
					update();
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					highlightEdgesEnabled = false;
					
					for(View<CyEdge> edgeView : MultiNetworkVariationPanel.this.dynet.getUnionNetworkView().getEdgeViews()){
						edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
					}
					MultiNetworkVariationPanel.this.dynet.getUnionNetworkView().updateView();

					for (CyNetworkView otherNetworkView : MultiNetworkVariationPanel.this.dynet.getMemberNetworkViews()){
						if (selectedNetworks.contains(otherNetworkView.getModel())){
							for (View<CyEdge> edgeView : otherNetworkView.getEdgeViews()){
								edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
							}
							otherNetworkView.updateView();
						}
					}
					
					
					CyTable edgeTable = MultiNetworkVariationPanel.this.dynet.getUnionNetwork().getDefaultEdgeTable();
					edgeTable.deleteColumn(EDGE_RESULT_COLUMN_NAME);
					
					
					MultiNetworkVariationPanel.this.controlPanel.releaseEdgeColourControl(MultiNetworkVariationPanel.this);
				}
			}
		});
		
		
		edgeProperty = DynamicNetwork.PRESENT;
		edgePropertyComboBox = new JComboBox<String>(ungroupedEdgeAttributes.toArray(new String[0]));
		edgePropertyComboBox.setSelectedIndex(ungroupedEdgeAttributes.indexOf(DynamicNetwork.PRESENT));
		edgePropertyComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					edgeProperty = (String)e.getItem();
					update();
				}
			}
		});
		
		
		
		update();  //initialize
		
		
		setBorder(new TitledBorder(new LineBorder(Color.BLACK), "Multiple Network Analysis", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 150, 30, 150, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 0, 0, 40, 0, 0, 0, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		chooseNetworksLabel = new JLabel("Choose included networks:");
		GridBagConstraints gbc_chooseNetworksLabel = new GridBagConstraints();
		gbc_chooseNetworksLabel.anchor = GridBagConstraints.WEST;
		gbc_chooseNetworksLabel.insets = new Insets(0, 0, 5, 5);
		gbc_chooseNetworksLabel.gridx = 1;
		gbc_chooseNetworksLabel.gridy = 1;
		add(chooseNetworksLabel, gbc_chooseNetworksLabel);
		
		networkListScrollPane = new JScrollPane();
		networkListScrollPane.getViewport().setBackground(Color.WHITE);
		
		separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.fill = GridBagConstraints.VERTICAL;
		gbc_separator.gridheight = 8;
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 2;
		gbc_separator.gridy = 1;
		add(separator, gbc_separator);
		
		GridBagConstraints gbc_highlightNodesCheckBox = new GridBagConstraints();
		gbc_highlightNodesCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_highlightNodesCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_highlightNodesCheckBox.gridx = 3;
		gbc_highlightNodesCheckBox.gridy = 1;
		add(highlightNodesCheckBox, gbc_highlightNodesCheckBox);
		
		GridBagConstraints gbc_networkListScrollPane = new GridBagConstraints();
		gbc_networkListScrollPane.gridheight = 7;
		gbc_networkListScrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_networkListScrollPane.fill = GridBagConstraints.BOTH;
		gbc_networkListScrollPane.gridx = 1;
		gbc_networkListScrollPane.gridy = 2;
		add(networkListScrollPane, gbc_networkListScrollPane);
		networkListScrollPane.setViewportView(networkList);
		
		nodePropertyLabel = new JLabel("Node property:");
		GridBagConstraints gbc_nodePropertyLabel = new GridBagConstraints();
		gbc_nodePropertyLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_nodePropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodePropertyLabel.gridx = 3;
		gbc_nodePropertyLabel.gridy = 2;
		add(nodePropertyLabel, gbc_nodePropertyLabel);
		
		
		GridBagConstraints gbc_nodePropertyComboBox = new GridBagConstraints();
		gbc_nodePropertyComboBox.anchor = GridBagConstraints.NORTH;
		gbc_nodePropertyComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_nodePropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodePropertyComboBox.gridx = 3;
		gbc_nodePropertyComboBox.gridy = 3;
		add(nodePropertyComboBox, gbc_nodePropertyComboBox);
		
		
	
		
		edgeWeightPropertyLabel = new JLabel("Edge weight property:");
		GridBagConstraints gbc_edgeWeightPropertyLabel = new GridBagConstraints();
		gbc_edgeWeightPropertyLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgeWeightPropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_edgeWeightPropertyLabel.gridx = 3;
		gbc_edgeWeightPropertyLabel.gridy = 4;
		add(edgeWeightPropertyLabel, gbc_edgeWeightPropertyLabel);
		
		GridBagConstraints gbc_edgeWeightPropertyComboBox = new GridBagConstraints();
		gbc_edgeWeightPropertyComboBox.anchor = GridBagConstraints.NORTH;
		gbc_edgeWeightPropertyComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_edgeWeightPropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_edgeWeightPropertyComboBox.gridx = 3;
		gbc_edgeWeightPropertyComboBox.gridy = 5;
		add(edgeWeightPropertyComboBox, gbc_edgeWeightPropertyComboBox);
		
		
		GridBagConstraints gbc_highlightEdgesCheckBox = new GridBagConstraints();
		gbc_highlightEdgesCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_highlightEdgesCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_highlightEdgesCheckBox.gridx = 3;
		gbc_highlightEdgesCheckBox.gridy = 6;
		add(highlightEdgesCheckBox, gbc_highlightEdgesCheckBox);
		
		
		edgePropertyLabel = new JLabel("Edge property:");
		GridBagConstraints gbc_edgePropertyLabel = new GridBagConstraints();
		gbc_edgePropertyLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgePropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_edgePropertyLabel.gridx = 3;
		gbc_edgePropertyLabel.gridy = 7;
		add(edgePropertyLabel, gbc_edgePropertyLabel);
		
		
		GridBagConstraints gbc_edgePropertyComboBox = new GridBagConstraints();
		gbc_edgePropertyComboBox.anchor = GridBagConstraints.NORTH;
		gbc_edgePropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_edgePropertyComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_edgePropertyComboBox.gridx = 3;
		gbc_edgePropertyComboBox.gridy = 8;
		add(edgePropertyComboBox, gbc_edgePropertyComboBox);
		
	}
	
	@Override
	public void update(){		
			
		if (highlightNodesEnabled){
			appAdapter.getTaskManager().execute(new TaskIterator(
					new NodeVarianceMapperTask(this, dynet, selectedNetworks, nodeProperty, edgeWeightProperty)));
		}
		
		if (highlightEdgesEnabled){
			appAdapter.getTaskManager().execute(new TaskIterator(
					new EdgeVarianceMapperTask(this, dynet, selectedNetworks, edgeProperty)));
		}
	}
	

	@Override
	public void stopNodeColourControl() {
		highlightNodesCheckBox.setSelected(false);
		
	}

	@Override
	public void stopEdgeColourControl() {
		highlightEdgesCheckBox.setSelected(false);
	}

	@Override
	public JPanel getEdgeInformationPanel(CyNode analyzedNode) {
		if (highlightEdgesEnabled){
			return new EdgeInformationPanel(dynet, selectedNetworks, edgeProperty, edgeVariationMap, analyzedNode);
		}else{
			return null;
		}
	}

	@Override
	public JPanel getNodeInformationPanel(CyNode analyzedNode) {
		if (highlightNodesEnabled){
			if (nodeProperty.equals(CONNECTIVITY)){
				return new NodeConnectivityInformationPanel(dynet, selectedNetworks, edgeWeightProperty, nodeVariationMap, analyzedNode);
			}else{
				return new NodePropertyInformationPanel(dynet, selectedNetworks, nodeProperty, nodeVariationMap, analyzedNode);
			}
		}else{
			return null;
		}
	}
	
	public void setEdgeMapperResult(Map<CyEdge, Double> edgeVariationMap){
		this.edgeVariationMap = edgeVariationMap;
		
		CyTable edgeTable = dynet.getUnionNetwork().getDefaultEdgeTable();
		edgeTable.deleteColumn(EDGE_RESULT_COLUMN_NAME);
		edgeTable.createColumn(EDGE_RESULT_COLUMN_NAME, Double.class, false);
		
		for (CyEdge edge : edgeVariationMap.keySet()){
			edgeTable.getRow(edge.getSUID()).set(EDGE_RESULT_COLUMN_NAME, edgeVariationMap.get(edge));
		}
	}
	
	
	//nodeDegreeMap is not null only when connectivity variance is being calculated
	public void setNodeMapperResult(boolean rewiring, Map<CyNode, Double> nodeVariationMap, Map<CyNode, Integer> nodeDegreeMap){
		this.nodeVariationMap = nodeVariationMap;
		this.nodeDegreeMap = nodeDegreeMap;
		
		CyTable nodeTable = dynet.getUnionNetwork().getDefaultNodeTable();
		nodeTable.deleteColumn(NODE_RESULT_COLUMN_NAME);
		nodeTable.deleteColumn(NODE_REWIRING_RESULT_COLUMN_NAME);
		nodeTable.deleteColumn(NODE_DEGREE_COLUMN_NAME);
		nodeTable.deleteColumn(NODE_CORRECTED_RESULT_COLUMN_NAME);
		
		
		if (rewiring){
			nodeTable.createColumn(NODE_REWIRING_RESULT_COLUMN_NAME, Double.class, false);
			nodeTable.createColumn(NODE_DEGREE_COLUMN_NAME, Integer.class, false);
			nodeTable.createColumn(NODE_CORRECTED_RESULT_COLUMN_NAME, Double.class, false);
		}else{
			nodeTable.createColumn(NODE_RESULT_COLUMN_NAME, Double.class, false);
		}
		
		
		for (CyNode node : nodeVariationMap.keySet()){
			double variance = nodeVariationMap.get(node);
			CyRow row = nodeTable.getRow(node.getSUID());
			
			if (rewiring){
				row.set(NODE_REWIRING_RESULT_COLUMN_NAME, variance);
				
				int degree = nodeDegreeMap.get(node);
				row.set(NODE_DEGREE_COLUMN_NAME, degree);
				if (degree != 0){
					row.set(NODE_CORRECTED_RESULT_COLUMN_NAME, variance / degree);
				}else{
					row.set(NODE_CORRECTED_RESULT_COLUMN_NAME, variance);
				}
			}else{
				row.set(NODE_RESULT_COLUMN_NAME, variance);
			}
		}
	}
}
