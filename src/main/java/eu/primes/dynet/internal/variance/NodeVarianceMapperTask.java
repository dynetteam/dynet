package eu.primes.dynet.internal.variance;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomNode;

/**
 * This task calculates variances in either node attribute or node connectivity across multiple networks. It then maps the
 * variance to node colour. Nodes that are more varying will be given stronger red colour.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeVarianceMapperTask extends AbstractTask{
	private MultiNetworkVariationPanel parent;
	private DynamicNetwork dynet;
	private List<CyNetwork> selectedNetworks;
	private String nodeProperty;
	private String edgeProperty;
	
	private HashMap<CyNode, Double> nodeVariationMap;
	private double minVariationAboveThreshold;
	private double maxVariationBelowThreshold;
	
	private HashMap<CyNode, Integer> nodeDegreeMap;
	
	private static final Color lowColor = DynamicNetwork.DEFAULT_NODE_COLOR;
	private static final Color highColor = Color.RED;
	
	
	public NodeVarianceMapperTask(MultiNetworkVariationPanel parent, DynamicNetwork dynet, List<CyNetwork> selectedNetworks, String nodeProperty, String edgeProperty){
		this.parent = parent;
		this.dynet = dynet;
		this.selectedNetworks = selectedNetworks;
		this.nodeProperty = nodeProperty;
		this.edgeProperty = edgeProperty;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("DyNet Node Variation Mapper");
		taskMonitor.setStatusMessage("Mapping node variation to node colour.");
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		//clearing previous highlighting. if selectedNetworks less than 2, clear all networks
		//otherwise, just clear networks that are not selected
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			if (!selectedNetworks.contains(memberNetworkView.getModel()) || selectedNetworks.size() < 2){
				for (View<CyNode> nodeView : memberNetworkView.getNodeViews()){
					nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
				}
				memberNetworkView.updateView();
			}
		}
		if (selectedNetworks.size() < 2){
			for (View<CyNode> nodeView : unionNetworkView.getNodeViews()){
				nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
			}
			unionNetworkView.updateView();
			
			return;  //no need to proceed with calculation if less than 2 networks are selected
		}
		
		
		if (nodeProperty.equals(MultiNetworkVariationPanel.CONNECTIVITY)){
			calculateConnectivityVariations();
			parent.setNodeMapperResult(true, nodeVariationMap, nodeDegreeMap);
		}else{
			calculatePropertyVariations();
			parent.setNodeMapperResult(false, nodeVariationMap, nodeDegreeMap);
		}
		
		findOutlierThresholds();
		
		
		for (CyNode node : unionNetwork.getNodeList()){
			Color highlighting = getColor(nodeVariationMap.get(node), minVariationAboveThreshold, maxVariationBelowThreshold);
			unionNetworkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, highlighting);
		
			for (CustomNode otherNode : dynet.getCorrespondingNodes(node, unionNetwork)){
				View<CyNode> otherNodeView = otherNode.getNodeView();
				if (otherNodeView != null && selectedNetworks.contains(otherNode.getNetwork())){
					otherNodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, highlighting);
				}
			}
		}
		
		unionNetworkView.updateView();
		for (CyNetworkView otherNetworkView : dynet.getMemberNetworkViews()){
			if (selectedNetworks.contains(otherNetworkView.getModel())){
				otherNetworkView.updateView();
			}
		}
	}
	
	
	private void calculatePropertyVariations() throws Exception{
		nodeDegreeMap = null;
		nodeVariationMap = new HashMap<CyNode, Double>();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		
		ArrayList<String> propertyNames = new ArrayList<String>();
		for (CyNetwork network : selectedNetworks){
			String propertyName = network.getRow(network).get(CyNetwork.NAME, String.class) + "_" + nodeProperty;
			if (unionNetwork.getDefaultNodeTable().getColumn(propertyName) == null){
				throw new Exception("Please choose an attribute present in all networks.");
			}
			propertyNames.add(propertyName);
		}
		
		
		for (CyNode node : unionNetwork.getNodeList()){
			ArrayList<Double> values = new ArrayList<Double>();
			for (String propertyName : propertyNames){
				Object value = unionNetwork.getRow(node).get(propertyName, Object.class);
				
				if (value == null){
					value = new Double(0);
				}else{
					Class<?> propertyType = unionNetwork.getDefaultNodeTable().getColumn(propertyName).getType();
					
					if (propertyType == Boolean.class){
						if ((Boolean)value) value = new Double(1);
						else value = new Double(0);
					}else if (propertyType == String.class){
						try{
							//attempts to parse string attributes. Should we just ban string attributes altogether?
							value = Double.parseDouble((String)value);
						}catch (Exception e){
							value = new Double(0);
						}
					}else{
						value = new Double(value.toString());
					}
				}

				if ((Double)value < 0){
					throw new Exception("Please choose an attribute containing only non-negative values.");
				}
				
				values.add((Double)value);
			}
			
			double variance = VarianceCalculator.normalizedVariance(values);
			nodeVariationMap.put(node, variance);
		}
	}
	
	
	
	
	private void calculateConnectivityVariations() throws Exception{
		nodeDegreeMap = new HashMap<CyNode, Integer>();
		nodeVariationMap = new HashMap<CyNode, Double>();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		
		ArrayList<Map<CyNode, Map<CyEdge, Double>>> nodeToEdgesMaps = new ArrayList<Map<CyNode, Map<CyEdge, Double>>>();
		for (CyNetwork network : selectedNetworks){
			nodeToEdgesMaps.add(getNodeToEdgesMap(dynet, network, edgeProperty));
		}
		
		for (CyNode node : unionNetwork.getNodeList()){
			ArrayList<List<Double>> vectors = new ArrayList<List<Double>>();
			ArrayList<CyEdge> edges = new ArrayList<CyEdge>(nodeToEdgesMaps.get(0).get(node).keySet());  //need a list to ensure that the order of edges remains the same
			nodeDegreeMap.put(node, edges.size());
			
			for (int i = 0; i < selectedNetworks.size(); i++){
				ArrayList<Double> vector = new ArrayList<Double>();
				vectors.add(vector);
				
				for (CyEdge edge : edges){
					vector.add(nodeToEdgesMaps.get(i).get(node).get(edge));
				}
			}
			
			double variance = VarianceCalculator.normalizedVectorVariance(vectors);
			nodeVariationMap.put(node, variance);
		}
	}
	
	
	private void findOutlierThresholds(){
		Collection<Double> values = nodeVariationMap.values();
		double mean = average(values);
		double stdDev = stdDev(values, mean);
		double variationLowThreshold = mean - (2 * stdDev);
		double variationHighThreshold = mean + (2 * stdDev);
		
		minVariationAboveThreshold = Double.POSITIVE_INFINITY;
		maxVariationBelowThreshold = 0;
		for (double value : values){
			if (value < minVariationAboveThreshold && value > variationLowThreshold){
				minVariationAboveThreshold = value;
			}
			if (value > maxVariationBelowThreshold && value < variationHighThreshold){
				maxVariationBelowThreshold = value;
			}
		}
	}
	
	
	
	private static double stdDev(Collection<Double> values, double average){
		double sum = 0;
		
		for (Double value : values){
			sum += (value - average) * (value - average);
		}
		
		if (values.size() == 0) return 0;
		return Math.sqrt(sum / (values.size() - 1));
	}
	
	private static double average(Collection<Double> values){
		double sum = 0;
		
		for (Double value : values){
			sum += value;
		}
		
		if (values.size() == 0) return 0;
		return sum/values.size();
	}
	
	
	private static Color getColor(double value, double lowThreshold, double highThreshold){
		double fractionDistance = rescale(value, lowThreshold, highThreshold, 0, 1);
		
		int r = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getRed(), highColor.getRed(), fractionDistance));
		int g = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getGreen(), highColor.getGreen(), fractionDistance));
		int b = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getBlue(), highColor.getBlue(), fractionDistance));
		
		return new Color(r,g,b);
	}
	
	
	private static double rescale(double value, double srcLow, double srcHigh, double destLow, double destHigh){
		if (value < srcLow) return destLow;
		if (value > srcHigh) return destHigh;
		
		double srcInterval = srcHigh - srcLow;
		if (srcInterval == 0) return (destLow + destHigh) / 2;
		
		double fraction = (value - srcLow) / srcInterval;
		return destLow + fraction * (destHigh - destLow);
	}
	
	
	//calculate points between two numbers that are a 'fraction' of the total distance
	//e.g. The point that is 0.6 of the distance between 11 and 15 = 11 + (0.6 * 4) = 13.4
	private static double fractionBetweenTwoNumbers(double start, double stop, double fraction){
		double interval = Math.abs(stop - start);
		double step = fraction * interval;
		
		if (stop > start) return start + step;
		else if (stop < start) return start - step;
		else return start;
	}
	
	
	
	/**
	 * Returns the mapping from each node to their edges (and values in the specified attribute) for a single member network
	 * The edges of a node will include both incoming and outgoing edges regardless of whether or not the networks are treated
	 * as directed/undirected networks.
	 * @throws Exception 
	 */
	public static Map<CyNode, Map<CyEdge, Double>> getNodeToEdgesMap(DynamicNetwork dynet, CyNetwork network, String weightAttribute) throws Exception{
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		HashMap<CyNode, Map<CyEdge, Double>> nodeToEdgesMap = new HashMap<CyNode, Map<CyEdge, Double>>();
		
		for (CyNode node : unionNetwork.getNodeList()){
			nodeToEdgesMap.put(node, new HashMap<CyEdge, Double>());
		}
		
		
		String columnName;
		if (dynet.getGroupedEdgeAttributes().contains(weightAttribute)){
			columnName = weightAttribute;
		}else{
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			columnName = networkName + "_" + weightAttribute;
		}
		
		
		CyColumn column = unionNetwork.getDefaultEdgeTable().getColumn(columnName);
		if (column == null) throw new Exception("Please choose weight attribute present in all networks.");
		Class<?> attributeType = attributeType = column.getType();
		
		
		for (CyEdge edge : unionNetwork.getEdgeList()){
			Object value = unionNetwork.getRow(edge).get(columnName, Object.class);
			if (value == null){  //if an edge is missing a data, just put the value 0.
				value = new Double(0);
			}else{
				if (attributeType == String.class){
					try{
						value = Double.parseDouble((String)value);
					}catch (Exception e){
						value = new Double(0);
					}
				}else if (attributeType == Boolean.class){
					if ((Boolean)value) value = new Double(1);
					else value = new Double(0);
				}else{
					value = new Double(value.toString());
				}
			}
			
			
			if ((Double)value < 0){
				throw new Exception("Please choose weight attribute containing only non-negative values.");
			}
			
			
			CyNode source = edge.getSource();
			CyNode target = edge.getTarget();
			
			nodeToEdgesMap.get(source).put(edge, (Double)value);
			nodeToEdgesMap.get(target).put(edge, (Double)value);		
		}
		
		return nodeToEdgesMap;
	}
}
