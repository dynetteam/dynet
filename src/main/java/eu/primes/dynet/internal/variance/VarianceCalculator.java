package eu.primes.dynet.internal.variance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class provides the utility methods for calculating both scalar variance and the vector variance
 * metric that we developed. Both scalar variance and vector variance are normalized, meaning all values are divided
 * by their mean first before variance is calculated. There's also an additional utility method for calculating
 * euclidean distance between two points in multi-dimensional space.
 * 
 * @author Ivan Hendy Goenawan
 */

public class VarianceCalculator {
	
	
	//There's probably a quicker way of calculating normalized variance, that is to calculate the variance first,
	//and then dividing the result with the square of the mean
	public static double normalizedVariance(List<Double> values){
		double averageNonZeroes = averageNonZeroes(values);
		
		for (int i = 0; i < values.size(); i++){
			if (averageNonZeroes != 0.0){
				values.set(i, values.get(i) / averageNonZeroes);
			}
		}
		
		double result = 0;
		double average = average(values);
		for (Double normalizedValue : values){
			result += (normalizedValue - average) * (normalizedValue - average);    
		}

		return result / (values.size() - 1);
	}
	
	//Description of vector variance (unnormalized version):
	//Just as scalar variance provides a measure of average distance from the mean in one dimension, the vector variance
	//provides a measure of average distance from the 'mean' in multi dimensions. The mean in this case is the centroid
	//point of the vectors. We calculate 'distance from mean' simply by measuring the euclidean distance from individual
	//vector to the 'mean' (centroid), similar to 'subtracting the mean' which we do for scalar variance.
	//
	//For the normalized version, each vector is first 'divided' by the centroid (component-wise), again similar to
	//the normalized scalar variance.
	//
	//Can anyone figure out the mathematical relation between the unnormalized vector variance, the centroid, and the
	//normalized vector variance? If we could figure this out, then the normalized vector variance could be calculated
	//more quickly, similar to the normalized scalar variance. If you could, I will give you a medal!
	public static double normalizedVectorVariance(List<List<Double>> vectors){
		List<Double> centroidNonZeroes = centroidNonZeroes(vectors);
		
		//normalize the vectors
		for (List<Double> vector : vectors){
			for (int i = 0; i < vector.size(); i++){
				if (centroidNonZeroes.get(i) != 0.0){
					vector.set(i, vector.get(i) / centroidNonZeroes.get(i));
				}
			}
		}
		
		List<Double> centroid = centroid(vectors);
		
		double result = 0;
		double distance = 0;
		for (List<Double> vector : vectors){
			distance = euclideanDistance(vector, centroid);
			result += distance * distance;
		}
		
		return result / (vectors.size() - 1);
	}
	
	
	private static double average(List<Double> values){
		double sum = 0;
		for (int i = 0; i < values.size(); i++){
			sum += values.get(i);
		}
		return sum / values.size();
	}
	
	
	private static double averageNonZeroes(List<Double> values){
		double sum = 0;
		int n = 0;
		for (int i = 0; i < values.size(); i++){
			if (values.get(i) != 0.0){
				sum += values.get(i);
				n++;
			}
		}
		
		if (n == 0.0) return 0.0;
		return sum / n;
	}
	
	private static List<Double> centroid(List<List<Double>> vectors){
		List<Double> centroid = new ArrayList<Double>(vectors.get(0));
		
		for (int i = 1; i < vectors.size(); i++){
			List<Double> vector = vectors.get(i);
			for (int j = 0; j < vector.size(); j++){
				centroid.set(j, centroid.get(j) + vector.get(j));
			}
		}
		
		for (int i = 0; i < centroid.size(); i++){
			centroid.set(i, centroid.get(i) / vectors.size());
		}
		
		return centroid;
	}
	
	
	private static List<Double> centroidNonZeroes(List<List<Double>> vectors){
		List<Double> centroid = new ArrayList<Double>(Collections.nCopies(vectors.get(0).size(), 0.0));
		List<Integer> n = new ArrayList<Integer>(Collections.nCopies(vectors.get(0).size(), 0));
		
		for (int i = 0; i < vectors.size(); i++){
			List<Double> vector = vectors.get(i);
			for (int j = 0; j < vector.size(); j++){
				if (vector.get(j) != 0.0){
					centroid.set(j, centroid.get(j) + vector.get(j));
					n.set(j, n.get(j) + 1);
				}
			}
		}
		
		for (int i = 0; i < centroid.size(); i++){
			if (n.get(i) != 0){
				centroid.set(i, centroid.get(i) / n.get(i));
			}
		}
		
		return centroid;
	}
	
	
	private static double euclideanDistance(List<Double> firstVector, List<Double> secondVector){
		int length = firstVector.size();
		double result = 0;
		
		for (int i = 0; i < length; i++){			
			result += (secondVector.get(i) - firstVector.get(i)) * (secondVector.get(i) - firstVector.get(i));
		}
		
		return Math.sqrt(result);
	}
	
}
