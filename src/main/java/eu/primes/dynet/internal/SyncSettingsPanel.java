package eu.primes.dynet.internal;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedEvent;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedListener;
import org.cytoscape.view.model.events.NetworkViewAddedEvent;
import org.cytoscape.view.model.events.NetworkViewAddedListener;

import eu.primes.dynet.internal.ControlPanel.ControlPanelComponent;

/**
 * A control panel component that controls the synchronization settings.
 * 
 * @author Ivan Hendy Goenawan
 */
public class SyncSettingsPanel extends ControlPanelComponent implements NetworkViewAddedListener, NetworkViewAboutToBeDestroyedListener{
	
	private DynamicNetwork dynet;
	private ControlPanel controlPanel;

	private JList<CyNetworkView> networkList;
	private List<CyNetwork> memberNetworks;
	private JCheckBox synchroniseLocationsCheckBox;
	private JCheckBox synchroniseSelectionsCheckBox;
	private JCheckBox synchronisePanZoomCheckBox;
	
	private boolean deletingNetworkView = false;   //hack so that listSelectionListener is not triggered when a network view is deleted (can cause error)
	
	
	
	public SyncSettingsPanel(DynamicNetwork dynet, ControlPanel controlPanel) {
		this.dynet = dynet;
		this.controlPanel = controlPanel;
		
		memberNetworks = dynet.getMemberNetworks();
		
		List<CyNetworkView> availableNetworkViews = dynet.getMemberNetworkViews();
		List<CyNetworkView> syncedNetworkViews = dynet.getSyncedNetworkViews();
		
		int[] syncedNetworkViewIndices = new int[syncedNetworkViews.size()];
		for (int i = 0; i < syncedNetworkViews.size(); i++){
			syncedNetworkViewIndices[i] = availableNetworkViews.indexOf(syncedNetworkViews.get(i));
		}
		
		networkList = new JList<CyNetworkView>(){
			@Override
			public boolean getScrollableTracksViewportHeight() {
				return false;
			}
		};
		DefaultListModel<CyNetworkView> listModel = new DefaultListModel<CyNetworkView>();
		for (CyNetworkView networkView : availableNetworkViews){
			listModel.addElement(networkView);
		}
		networkList.setModel(listModel);
		networkList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		networkList.setSelectedIndices(syncedNetworkViewIndices);
		networkList.setCellRenderer(new DefaultListCellRenderer(){
			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {	
				CyNetworkView networkView = (CyNetworkView) value;
				String networkName = networkView.getModel().toString();
				return super.getListCellRendererComponent(list, networkName, index, isSelected,
						cellHasFocus);
			}
		});
		networkList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false && !deletingNetworkView){
					ArrayList<CyNetworkView> added = new ArrayList<CyNetworkView>();
					ArrayList<CyNetworkView> removed = new ArrayList<CyNetworkView>();
					
					for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++){
						if(networkList.isSelectedIndex(i)){
							added.add(networkList.getModel().getElementAt(i));
						}else{
							removed.add(networkList.getModel().getElementAt(i));
						}
					}
					
					if (!added.isEmpty()) SyncSettingsPanel.this.dynet.addSyncedNetworkViews(added);
					if (!removed.isEmpty()) SyncSettingsPanel.this.dynet.removeSyncedNetworkViews(removed);
				}
				
				deletingNetworkView = false;
			}
		});
		
		//add this listener so that items in the list can be reordered using drag and drop
		MouseAdapter reorderListener = new MouseAdapter() {
			private int pressIndex = 0;
			private int releaseIndex = 0;
			
			@Override
			public void mousePressed(MouseEvent e) {
				pressIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
			}
	
			@Override
			public void mouseReleased(MouseEvent e) {
				releaseIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
				if (releaseIndex != pressIndex && releaseIndex != -1) {
					DefaultListModel model = (DefaultListModel) ((JList)e.getSource()).getModel();
					Object item = model.elementAt(pressIndex);
					model.removeElementAt(pressIndex);
					model.insertElementAt(item, releaseIndex);
				}
			}
	
			@Override
			public void mouseDragged(MouseEvent e) {
				mouseReleased(e);
				pressIndex = releaseIndex;
			}
		};
		networkList.addMouseListener(reorderListener);
		networkList.addMouseMotionListener(reorderListener);
		
		
		synchronisePanZoomCheckBox = new JCheckBox("Synchronise network pan/zoom");
		synchronisePanZoomCheckBox.setSelected(dynet.getSyncPanZoomEnabled());
		synchronisePanZoomCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					SyncSettingsPanel.this.dynet.setSyncPanZoomEnabled(true);
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					SyncSettingsPanel.this.dynet.setSyncPanZoomEnabled(false);
				}
			}
		});
		
		
		synchroniseLocationsCheckBox = new JCheckBox("Synchronise node locations");
		synchroniseLocationsCheckBox.setSelected(dynet.getSyncLocationsEnabled());
		synchroniseLocationsCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					SyncSettingsPanel.this.dynet.setSyncLocationsEnabled(true);
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					SyncSettingsPanel.this.dynet.setSyncLocationsEnabled(false);
				}
			}
		});
		
		synchroniseSelectionsCheckBox = new JCheckBox("Synchronise node/edge selections");
		synchroniseSelectionsCheckBox.setSelected(dynet.getSyncSelectionsEnabled());
		synchroniseSelectionsCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					SyncSettingsPanel.this.dynet.setSyncSelectionsEnabled(true);
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					SyncSettingsPanel.this.dynet.setSyncSelectionsEnabled(false);
				}
			}
		});
		
		
		
		setBorder(new TitledBorder(new LineBorder(Color.BLACK), "Network-View Synchronisation Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 150, 10, 0, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 0, 0, 50, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel chooseSyncedNetworksLabel = new JLabel("Choose synced networks:");
		GridBagConstraints gbc_chooseSyncedNetworksLabel = new GridBagConstraints();
		gbc_chooseSyncedNetworksLabel.anchor = GridBagConstraints.WEST;
		gbc_chooseSyncedNetworksLabel.insets = new Insets(0, 0, 5, 5);
		gbc_chooseSyncedNetworksLabel.gridx = 1;
		gbc_chooseSyncedNetworksLabel.gridy = 1;
		add(chooseSyncedNetworksLabel, gbc_chooseSyncedNetworksLabel);
		
		JScrollPane networkListScrollPane = new JScrollPane();
		networkListScrollPane.getViewport().setBackground(Color.WHITE);
		GridBagConstraints gbc_networkListScrollPane = new GridBagConstraints();
		gbc_networkListScrollPane.gridheight = 4;
		gbc_networkListScrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_networkListScrollPane.fill = GridBagConstraints.BOTH;
		gbc_networkListScrollPane.gridx = 1;
		gbc_networkListScrollPane.gridy = 2;
		add(networkListScrollPane, gbc_networkListScrollPane);
		networkListScrollPane.setViewportView(networkList);
		
		
		GridBagConstraints gbc_synchronisePanZoomCheckBox = new GridBagConstraints();
		gbc_synchronisePanZoomCheckBox.anchor = GridBagConstraints.WEST;
		gbc_synchronisePanZoomCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_synchronisePanZoomCheckBox.gridx = 3;
		gbc_synchronisePanZoomCheckBox.gridy = 2;
		add(synchronisePanZoomCheckBox, gbc_synchronisePanZoomCheckBox);
		
		
		GridBagConstraints gbc_synchroniseLocationsCheckBox = new GridBagConstraints();
		gbc_synchroniseLocationsCheckBox.anchor = GridBagConstraints.WEST;
		gbc_synchroniseLocationsCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_synchroniseLocationsCheckBox.gridx = 3;
		gbc_synchroniseLocationsCheckBox.gridy = 3;
		add(synchroniseLocationsCheckBox, gbc_synchroniseLocationsCheckBox);
		
		
		GridBagConstraints gbc_synchroniseSelectionsCheckBox = new GridBagConstraints();
		gbc_synchroniseSelectionsCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_synchroniseSelectionsCheckBox.gridx = 3;
		gbc_synchroniseSelectionsCheckBox.gridy = 4;
		add(synchroniseSelectionsCheckBox, gbc_synchroniseSelectionsCheckBox);
	}

	
	@Override
	public void update() {
		//do nothing. This component doesn't directly control the appearance of networks.
	}

	/**
	 * Listens to when a network view is deleted and remove it from the network view selection.
	 */
	@Override
	public void handleEvent(NetworkViewAboutToBeDestroyedEvent e) {
		deletingNetworkView = true;
		((DefaultListModel<CyNetworkView>)networkList.getModel()).removeElement(e.getNetworkView());
		dynet.removeSyncedNetworkViews(Arrays.asList(e.getNetworkView()));
	}

	/**
	 * Listens to when a network view is added and add it to the selection.
	 */
	@Override
	public void handleEvent(final NetworkViewAddedEvent e) {
		if (memberNetworks.contains(e.getNetworkView().getModel())){
			
			//when a networkView is first created, Cytoscape automatically apply the preferred network layout
			//That's why we need to delay activating the sync for the newly created network view
			
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	((DefaultListModel<CyNetworkView>)networkList.getModel()).addElement(e.getNetworkView());
			    			int index = ((DefaultListModel<CyNetworkView>)networkList.getModel()).indexOf(e.getNetworkView());
			    			networkList.addSelectionInterval(index, index);
			            }
			        }, 
			        2000 
			);
		}
	}

}
