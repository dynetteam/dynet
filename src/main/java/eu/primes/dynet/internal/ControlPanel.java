package eu.primes.dynet.internal;

import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.CytoPanelComponent2;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNode;

/**
 * This is the main panel which controls the numerous functions of the application. However this class itself
 * is only a container to which additional components can be added or removed. This makes it easy if we want to
 * extend the functionality of the application in the future.
 * 
 * Only one of these component is allowed to control the color of nodes at any one time (and the same is true for edge color)
 * to provide collision.
 * 
 * @author Ivan Hendy Goenawan
 */
public class ControlPanel extends JScrollPane implements CytoPanelComponent2 {

	public static final String IDENTIFIER = "eu.primes.dynet.controlpanel";
	
	private DynamicNetwork dynet;
	private CySwingAppAdapter appAdapter;
	private JPanel mainPanel;
	
	private NodeColourController nodeColourController;
	private EdgeColourController edgeColourController;
	
	public ControlPanel (DynamicNetwork dynet, CySwingAppAdapter appAdapter){
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		
		mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		setViewportView(mainPanel);
	}
	
	public void addToMainPanel(ControlPanelComponent panel){
		mainPanel.add(panel);
	}
	
	public void addToMainPanelAt(ControlPanelComponent panel, int index){
		mainPanel.add(panel, index);
	}
	
	public void removeFromMainPanel(ControlPanelComponent panel){
		mainPanel.remove(panel);
	}
	
	/**
	 * This provides a method for temporarily replacing the whole panel with another panel. This is suitable for displaying
	 * transient information or when a user enters a different 'mode' temporarily.
	 */
	public void replaceMainPanel(JPanel newPanel){
		setViewportView(newPanel);
	}
	
	/**
	 * Resets the main panel to its original
	 */
	public void resetMainPanel(){
		setViewportView(mainPanel);
	}
	
	public JPanel getCurrentMainPanel(){
		return (JPanel)getViewport().getView();
	}
	
	/**
	 * Call this when you want the current settings to be reapplied. The panel will then go through each of its components
	 * and call their update method.
	 */
	public void updateView(){
		for (int i = 0; i < mainPanel.getComponentCount(); i++){
			ControlPanelComponent panel = (ControlPanelComponent) mainPanel.getComponent(i);
			panel.update();
		}
	}
	
	/**
	 * Call this when a component wants to take control of node coloring. The panel will notify the previous controller
	 * that the control has been taken away from it. 
	 */
	public void takeNodeColourControl(NodeColourController newController){
		if (nodeColourController != null && nodeColourController != newController){
			nodeColourController.stopNodeColourControl();
		}
		nodeColourController = newController;
	}
	
	/**
	 * Call this when a component wants to take control of edge coloring. The panel will notify the previous controller
	 * that the control has been taken away from it.
	 */
	public void takeEdgeColourControl(EdgeColourController newController){
		if (edgeColourController != null && edgeColourController != newController){
			edgeColourController.stopEdgeColourControl();
		}
		edgeColourController = newController;
	}
	
	/**
	 * Call this when a component wants to release its control of node coloring.
	 */
	public void releaseNodeColourControl(NodeColourController controller){
		if (nodeColourController == controller){
			nodeColourController = null;
		}
	}
	
	/**
	 * Call this when a component wants to release its control of edge coloring.
	 */
	public void releaseEdgeColourControl(EdgeColourController controller){
		if (edgeColourController == controller){
			edgeColourController = null;
		}
	}
	
	/**
	 * When a component is controlling edge color, it has to be able to return a panel displaying relevant information about
	 * the coloring scheme that it uses. This method retrieve that panel.
	 */
	public JPanel getEdgeInformationPanel(CyNode analyzedNode){
		if (edgeColourController == null){
			return null;
		}else{
			return edgeColourController.getEdgeInformationPanel(analyzedNode);
		}
	}
	
	/**
	 * When a component is controlling node color, it has to be able to return a panel displaying relevant information about
	 * the coloring scheme that it uses. This method retrieve that panel. 
	 */
	public JPanel getNodeInformationPanel(CyNode analyzedNode){
		if (nodeColourController == null){
			return null;
		}else{
			return nodeColourController.getNodeInformationPanel(analyzedNode);
		}
	}
	
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}

	@Override
	public String getTitle() {
		return "DyNet";
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	@Override
	public String getIdentifier() {
		return IDENTIFIER;
	}

	
	/**
	 * Components that might want to control the coloring of nodes has to implement this interface.
	 * They have to be able to provide a panel that displays the relevant information about the coloring
	 * scheme that they use, and also provide a method for notifying them if the control of node color
	 * has been taken away from them.
	 */
	public interface NodeColourController{
		public JPanel getNodeInformationPanel(CyNode analyzedNode);
		public void stopNodeColourControl();
	}
	
	/**
	 * Components that might want to control the coloring of edges has to implement this interface.
	 * They have to be able to provide a panel that displays the relevant information about the coloring
	 * scheme that they use, and also provide a method for notifying them if they control of edge color
	 * has been taken away from them. 
	 */
	public interface EdgeColourController{
		public JPanel getEdgeInformationPanel(CyNode analyzedNode);
		public void stopEdgeColourControl();
	}
	
	/**
	 * All components that are to be added to the control panel has to extend the ControlPanelComponent class.
	 * They need to provide an implementation of the update method which should reapply their current settings
	 * to the networks.
	 */
	public static abstract class ControlPanelComponent extends JPanel{
		public abstract void update();
	}
}
