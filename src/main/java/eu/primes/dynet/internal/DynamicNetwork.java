package eu.primes.dynet.internal;

import java.awt.Color;
import java.awt.Paint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.events.RowSetRecord;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.model.events.ViewChangeRecord;
import org.cytoscape.view.model.events.ViewChangedEvent;
import org.cytoscape.view.model.events.ViewChangedListener;
import org.cytoscape.view.presentation.property.ArrowShapeVisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.LineTypeVisualProperty;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.PassthroughMapping;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.TaskIterator;

/** 
 * This is the main class of the application which provides a data structure for the dynamic network.
 * It stores references to the member networks and the union network, and provide methods for finding
 * corresponding nodes / edges.
 * 
 * Currently, this class also directly handles the synchronization between the different networkviews,  
 * but this functionality can be separated into a different class in the future.
 * 
 * @author Ivan Hendy Goenawan
 */
public class DynamicNetwork implements ViewChangedListener, RowsSetListener {
	private CyAppAdapter appAdapter;
	private CyNetwork unionNetwork;
	private List<CyNetwork> memberNetworks;
	private CyNetworkView unionNetworkView;
	private VisualStyle unionNetworkVisualStyle;
	private List<String> nodeAttributeGroups;	   //contains attributes that are used as the criteria for identifying corresponding nodes
	private List<String> edgeAttributeGroups;      //contains attributes that are used as the criteria for identifying corresponding edges
	private Set<String> nodeAttributesUngrouped;   //contains node attributes that are not grouped but without network name prefix
	private Set<String> edgeAttributesUngrouped;   //contains edge attributes that are not grouped but without network name prefix
	private HashMap<CyColumn, CyNetwork> columnToNetworkMap;  //for checking which network a column belongs to. Null if the column doesn't belong to a particular member network
	private boolean directed;
	private Timer updateViewTimer;
	private TimerTask updateViewTimerTask;
	
	//These provide ways for finding corresponding nodes and edges. All corresponding nodes and edges from the different member networks
	//are given the same ID, which is the SUID of their corresponding node/edge in the union network.
	private HashMap<Long, List<CustomNode>> idToNodes;	
	private HashMap<Long, List<CustomEdge>> idToEdges;
	private HashMap<CyNode, Long> nodeToId;
	private HashMap<CyEdge, Long> edgeToId;
	
	
	private List<CyNetworkView> syncedNetworkViews;
	private boolean syncPanZoomEnabled;
	private boolean syncLocationsEnabled;
	private boolean syncSelectionsEnabled;
	
	
	public static final String PRESENT = "present";		// node and edge attribute that specify their presence in a particular network 
	public static final Color DEFAULT_NODE_COLOR = Color.WHITE;
	public static final Color DEFAULT_EDGE_COLOR = Color.GRAY;
	public static final double DEFAULT_NODE_SIZE = 30.0;
	public static final double DEFAULT_EDGE_WIDTH = 3.0;
	
	
	/**
	 * Construct the dynamic network.
	 * 
	 * The nodeAttributeGroups and edgeAttributeGroups parameter contain the attributes that will be used for identifying corresponding
	 * node/edges in the member networks.
	 * 
	 * The treatAsDirected parameter specifies whether or not to treat the networks as directed networks.
	 * @throws Exception 
	 *
	 */
	public DynamicNetwork(CyAppAdapter appAdapter, List<CyNetwork> memberNetworks, List<String> nodeAttributeGroups, List<String> edgeAttributeGroups, boolean treatAsDirected, CyLayoutAlgorithm defaultLayout) throws Exception{
		this.appAdapter = appAdapter;
		this.updateViewTimer = new Timer();
		this.memberNetworks = memberNetworks;
		this.nodeAttributeGroups = nodeAttributeGroups;
		this.edgeAttributeGroups = edgeAttributeGroups;
		this.directed = treatAsDirected;
		
		//check if all networks have unique names
		HashSet<String> networkNames = new HashSet<String>();
		for (CyNetwork network : memberNetworks){
			if (!networkNames.add(network.getRow(network).get(CyNetwork.NAME, String.class))){
				throw new Exception("Duplicate network names are present. Please rename the networks first.");
			}
		}
		
		
		this.unionNetwork = createUnionNetwork(memberNetworks, directed);
		this.unionNetworkView = layoutAllNetworks(defaultLayout);
		this.unionNetworkVisualStyle = setupVisualStyle(unionNetworkView, directed);
		appAdapter.getCyNetworkManager().addNetwork(unionNetwork);
		appAdapter.getCyNetworkViewManager().addNetworkView(unionNetworkView);
		
		
		syncedNetworkViews = getMemberNetworkViews();
		syncPanZoomEnabled = true;
		syncLocationsEnabled = true;
		syncSelectionsEnabled = true;
	}
	
	public CyNetwork getUnionNetwork(){
		return unionNetwork;
	}
	
	public CyNetworkView getUnionNetworkView(){
		return unionNetworkView;
	}
	
	public List<CyNetwork> getMemberNetworks(){
		return memberNetworks;
	}
	
	public List<CyNetworkView> getMemberNetworkViews(){
		ArrayList<CyNetworkView> memberNetworkViews = new ArrayList<CyNetworkView>();
		for (CyNetwork member : memberNetworks){
			Collection<CyNetworkView> networkViews = appAdapter.getCyNetworkViewManager().getNetworkViews(member);
			if (!networkViews.isEmpty()){
				memberNetworkViews.add(networkViews.iterator().next());
			}
		}
		return memberNetworkViews;
	}
	
	public CyNetwork getContainingNetwork(CyNode node){
		if (unionNetwork.containsNode(node)){
			return unionNetwork;
		}
		for (CyNetwork network : memberNetworks){
			if (network.containsNode(node)){
				return network;
			}
		}
		return null;
	}
	
	private CyNetwork createUnionNetwork(List<CyNetwork> memberNetworks, boolean directed){
		CyNetwork unionNetwork = appAdapter.getCyNetworkFactory().createNetwork();
		unionNetwork.getRow(unionNetwork).set(CyNetwork.NAME, "DyNet Central Reference Network");
		CyTable unionNetworkNodeTable = unionNetwork.getDefaultNodeTable();
		CyTable unionNetworkEdgeTable = unionNetwork.getDefaultEdgeTable();
		
		idToNodes = new HashMap<Long, List<CustomNode>>();
		idToEdges = new HashMap<Long, List<CustomEdge>>();
		nodeToId = new HashMap<CyNode, Long>();
		edgeToId = new HashMap<CyEdge, Long>();
		
		nodeAttributesUngrouped = new HashSet<String>();
		edgeAttributesUngrouped = new HashSet<String>();
		columnToNetworkMap = new HashMap<CyColumn, CyNetwork>();
		
		//copying the content of each member network one by one into the union network.
		for (CyNetwork network : memberNetworks){
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			
			//copying missing node attributes into the node table of the union network....
			//'grouped' attributes (used for identifying corresponding nodes) will retain the same name
			//other attributes will be named as networkName_attributeName
			Collection<CyColumn> nodeAttributes = network.getDefaultNodeTable().getColumns();
			Iterator<CyColumn> nodeAttributeIterator = nodeAttributes.iterator();
			
			while (nodeAttributeIterator.hasNext()){
				CyColumn column = nodeAttributeIterator.next();
				String columnName = column.getName();
				
				if (!columnName.equals(CyNetwork.SELECTED) && !columnName.equals(CyNetwork.SUID) && !columnName.equals("shared name")){
					if (nodeAttributeGroups.contains(columnName)){
						if (unionNetworkNodeTable.getColumn(columnName) == null){
							unionNetworkNodeTable.createColumn(columnName, column.getType(), true, null);
							columnToNetworkMap.put(unionNetworkNodeTable.getColumn(columnName), null);
						}
					}else{
						unionNetworkNodeTable.createColumn(networkName + "_" + columnName, column.getType(), true, null);
						nodeAttributesUngrouped.add(columnName);
						columnToNetworkMap.put(unionNetworkNodeTable.getColumn(networkName + "_" + columnName), network);
					}
				}
			}
			
			//add "present" attribute to indicate presence of nodes
			unionNetworkNodeTable.createColumn(networkName + "_" + PRESENT, Boolean.class, true, false);
			nodeAttributesUngrouped.add(PRESENT);
			columnToNetworkMap.put(unionNetworkNodeTable.getColumn(networkName + "_" + PRESENT), network);
			
			
			//copying missing nodes
			List<CyNode> networkNodes = network.getNodeList();
			for (CyNode node : networkNodes){
				Collection<CyRow> matchingRows = new ArrayList<CyRow>();
				
				//find corresponding node that's already in the union network
				if (nodeAttributeGroups.size() != 0){
					matchingRows.addAll(unionNetworkNodeTable.getAllRows());
					for (String nodeAttribute : nodeAttributeGroups){
						matchingRows = getMatchingRows(matchingRows, nodeAttribute, network.getRow(node).get(nodeAttribute, Object.class));
						if (matchingRows.size() == 0) break;
					}
				}
				
				CyNode correspondingNode;
				if (matchingRows.size() > 0){
					correspondingNode = unionNetwork.getNode(matchingRows.iterator().next().get(CyNetwork.SUID, Long.class));
				}else{
					//if a corresponding node isn't already in the unionNetwork, create a new node
					correspondingNode = unionNetwork.addNode();
					idToNodes.put(correspondingNode.getSUID(), new ArrayList<CustomNode>());
					idToNodes.get(correspondingNode.getSUID()).add(new CustomNode(correspondingNode.getSUID(), correspondingNode, unionNetwork));
					nodeToId.put(correspondingNode, correspondingNode.getSUID());
				}
				nodeToId.put(node, correspondingNode.getSUID());
				idToNodes.get(correspondingNode.getSUID()).add(new CustomNode(correspondingNode.getSUID(), node, network));
				

				
				//copying attribute data for this particular node
				nodeAttributeIterator = nodeAttributes.iterator();
				while (nodeAttributeIterator.hasNext()){
					CyColumn column = nodeAttributeIterator.next();
					String columnName = column.getName();
					String newColumnName = networkName + "_" + columnName;
					
					if (!columnName.equals(CyNetwork.SELECTED) && !columnName.equals(CyNetwork.SUID) && !columnName.equals("shared name")){
						if (nodeAttributeGroups.contains(columnName)){
							unionNetwork.getRow(correspondingNode).set(columnName,
									network.getRow(node).get(columnName, column.getType()));
						}else{
							unionNetwork.getRow(correspondingNode).set(newColumnName,
									network.getRow(node).get(columnName, column.getType()));
							if (columnName.equals(CyNetwork.NAME)){
								unionNetwork.getRow(correspondingNode).set(columnName,
										network.getRow(node).get(columnName, column.getType()));
							}
						}
					}
				}
				
				unionNetwork.getRow(correspondingNode).set(networkName + "_" + PRESENT, true);
			}
			
			
			//copying missing edge attributes into the edge table of the union network....
			//'grouped' attributes (used for identifying corresponding edges) will retain the same name
			//other attributes will be named as networkName_attributeName
			Collection<CyColumn> edgeAttributes = network.getDefaultEdgeTable().getColumns();
			Iterator<CyColumn> edgeAttributeIterator = edgeAttributes.iterator();
			
			while (edgeAttributeIterator.hasNext()){
				CyColumn column = edgeAttributeIterator.next();
				String columnName = column.getName();
				
				if (!columnName.equals(CyNetwork.SELECTED) && !columnName.equals(CyNetwork.SUID) 
						&& !columnName.equals("shared name") && !columnName.equals("shared interaction")){
					if (edgeAttributeGroups.contains(columnName)){
						if (unionNetworkEdgeTable.getColumn(columnName) == null){
							unionNetworkEdgeTable.createColumn(columnName, column.getType(), true, null);
							columnToNetworkMap.put(unionNetworkEdgeTable.getColumn(columnName), null);
						}
					}else{
						unionNetworkEdgeTable.createColumn(networkName + "_" + columnName, column.getType(), true, null);
						edgeAttributesUngrouped.add(columnName);
						columnToNetworkMap.put(unionNetworkEdgeTable.getColumn(networkName + "_" + columnName), network);
					}
				}
			}
			
			//add "present" attribute to indicate presence of edges
			unionNetworkEdgeTable.createColumn(networkName + "_" + PRESENT, Boolean.class, true, false);
			edgeAttributesUngrouped.add(PRESENT);
			columnToNetworkMap.put(unionNetworkEdgeTable.getColumn(networkName + "_" + PRESENT), network);
			
			
			//copying missing edges
			List<CyEdge> networkEdges = network.getEdgeList();
			
			for (CyEdge edge : networkEdges){
				CyNode source = edge.getSource();
				CyNode target = edge.getTarget();
				
				CyNode correspondingSource = getCorrespondingNodes(source, network).get(0).getNode();
				CyNode correspondingTarget = getCorrespondingNodes(target, network).get(0).getNode();
				
				
				//corresponding edge is first and foremost determined by looking at its source and target node, taking into account
				//whether or not the network is treated as directed / undirected.
				List<CyEdge> correspondingEdges = unionNetwork.getConnectingEdgeList(correspondingSource, correspondingTarget, Type.ANY);
				Collection<CyRow> matchingRows = new ArrayList<CyRow>();
				for (CyEdge correspondingEdge : correspondingEdges){
					if (directed && correspondingEdge.getSource() != correspondingSource){
						//skip
					}else{
						matchingRows.add(unionNetwork.getRow(correspondingEdge));
					}
				}
				//matching edges are then further filtered based on the attributes that user has chosen for identifying corresponding edges
				//this might include, for example, the type of interaction, etc.
				for (String edgeAttribute : edgeAttributeGroups){
					matchingRows = getMatchingRows(matchingRows, edgeAttribute, network.getRow(edge).get(edgeAttribute, Object.class));
					if (matchingRows.size() == 0) break;
				}
				
				CyEdge correspondingEdge;
				if (matchingRows.size() > 0){
					correspondingEdge = unionNetwork.getEdge(matchingRows.iterator().next().get(CyNetwork.SUID, Long.class));
				}else{
					//if a corresponding edge is not already found in the union network, create the edge
					//we need to make the edges directed to prevent this bug http://code.cytoscape.org/redmine/issues/3255
					correspondingEdge = unionNetwork.addEdge(correspondingSource, correspondingTarget, true /*edge.isDirected()*/);
					idToEdges.put(correspondingEdge.getSUID(), new ArrayList<CustomEdge>());
					idToEdges.get(correspondingEdge.getSUID()).add(new CustomEdge(correspondingEdge.getSUID(), correspondingEdge, unionNetwork));
					edgeToId.put(correspondingEdge, correspondingEdge.getSUID());
				}
				edgeToId.put(edge, correspondingEdge.getSUID());
				idToEdges.get(correspondingEdge.getSUID()).add(new CustomEdge(correspondingEdge.getSUID(), edge, network));


				
				//copying attribute data for this particular edge
				edgeAttributeIterator = edgeAttributes.iterator();
				while (edgeAttributeIterator.hasNext()){
					CyColumn column = edgeAttributeIterator.next();
					String columnName = column.getName();
					String newColumnName = networkName + "_" + columnName;
					
					if (!columnName.equals(CyNetwork.SELECTED) && !columnName.equals(CyNetwork.SUID) && 
							!columnName.equals("shared name") && !columnName.equals("shared interaction")){	
						if (edgeAttributeGroups.contains(columnName)){
							unionNetwork.getRow(correspondingEdge).set(columnName,
									network.getRow(edge).get(columnName, column.getType()));
						}else{
							unionNetwork.getRow(correspondingEdge).set(newColumnName,
									network.getRow(edge).get(columnName, column.getType()));
							if (columnName.equals(CyNetwork.NAME) || columnName.equals(CyEdge.INTERACTION)){
								unionNetwork.getRow(correspondingEdge).set(columnName,
										network.getRow(edge).get(columnName, column.getType()));
							}
						}
					}
				}
				
				unionNetwork.getRow(correspondingEdge).set(networkName + "_" + PRESENT, true);
			}
		}
		
		return unionNetwork;
	}
	
	private CyNetworkView layoutAllNetworks(CyLayoutAlgorithm algor){
		CyNetworkView unionNetworkView = appAdapter.getCyNetworkViewFactory().createNetworkView(unionNetwork);
		
		//ensure that all member network has a network view available
		for (CyNetwork member : getMemberNetworks()){
			if (appAdapter.getCyNetworkViewManager().getNetworkViews(member).isEmpty()){
				CyNetworkView memberNetworkView = appAdapter.getCyNetworkViewFactory().createNetworkView(member);
				appAdapter.getCyNetworkViewManager().addNetworkView(memberNetworkView);
			}
		}
		
		//apply the user-chosen layout algorithm
		if (algor == null){
			algor = appAdapter.getCyLayoutAlgorithmManager().getDefaultLayout();
		}
		
		SynchronousTaskManager<?> synTaskMan = appAdapter.getCyServiceRegistrar().getService(SynchronousTaskManager.class);           
		synTaskMan.execute(algor.createTaskIterator(unionNetworkView, algor.createLayoutContext(), CyLayoutAlgorithm.ALL_NODE_VIEWS, null));
		
		//lay out corresponding nodes and edges at the same locations.
		List<CyNode> allNodes = unionNetwork.getNodeList();
		for (CyNode node : allNodes){
			double x = unionNetworkView.getNodeView(node).getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
			double y = unionNetworkView.getNodeView(node).getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);
			
			for (CustomNode corresponding : getCorrespondingNodes(node, unionNetwork)){
				View<CyNode> nodeView = corresponding.getNodeView();
				if (nodeView != null){
					nodeView.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
					nodeView.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
				}
			}
		}
		
		
		for (CyNetworkView networkView : getMemberNetworkViews()){
			networkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION));
			networkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION));
			networkView.setVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR));
		}
		
		return unionNetworkView;
	}
	
	private VisualStyle setupVisualStyle(final CyNetworkView unionNetworkView, boolean directed){
		final VisualStyle dynetVisualStyle = appAdapter.getVisualStyleFactory().createVisualStyle("Dynet");
		appAdapter.getVisualMappingManager().addVisualStyle(dynetVisualStyle);
		
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NETWORK_BACKGROUND_PAINT, Color.BLACK);
		
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, 0.0);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, DEFAULT_NODE_COLOR);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_SELECTED_PAINT, Color.YELLOW);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_SIZE, DEFAULT_NODE_SIZE);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLUE);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 12);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.NODE_VISIBLE, true);
		
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_LINE_TYPE, LineTypeVisualProperty.SOLID);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_WIDTH, DEFAULT_EDGE_WIDTH);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, DEFAULT_EDGE_COLOR);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_STROKE_SELECTED_PAINT, Color.ORANGE);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_TRANSPARENCY, 255);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_VISIBLE, true);
		dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_SOURCE_ARROW_SHAPE, ArrowShapeVisualProperty.NONE);
		
		if (directed){
			dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_TARGET_ARROW_SHAPE, ArrowShapeVisualProperty.DELTA);
			VisualLexicon lexicon = appAdapter.getCyServiceRegistrar().getService(VisualLexicon.class);
			VisualProperty<Paint> edgeTargetArrowPaint = (VisualProperty<Paint>) lexicon.lookup(CyEdge.class, "edgeTargetArrowColor");
			dynetVisualStyle.setDefaultValue(edgeTargetArrowPaint, Color.GRAY);
		}else{
			dynetVisualStyle.setDefaultValue(BasicVisualLexicon.EDGE_TARGET_ARROW_SHAPE, ArrowShapeVisualProperty.NONE);
		}
		
		VisualMappingFunctionFactory passthroughMappingFactory = appAdapter.getVisualMappingFunctionPassthroughFactory();
		PassthroughMapping<String, String> nodeNameMapping = (PassthroughMapping<String, String>) passthroughMappingFactory.createVisualMappingFunction(CyNetwork.NAME, String.class, BasicVisualLexicon.NODE_LABEL);
		dynetVisualStyle.addVisualMappingFunction(nodeNameMapping);
		
		PassthroughMapping<String, String> nodeTooltipMapping = (PassthroughMapping<String, String>) passthroughMappingFactory.createVisualMappingFunction(CyNetwork.NAME, String.class, BasicVisualLexicon.NODE_TOOLTIP);
		dynetVisualStyle.addVisualMappingFunction(nodeTooltipMapping);

		
		//clear all previous locks from member networks so they will all look the same
		for (CyNetworkView memberNetworkView : getMemberNetworkViews()){
			memberNetworkView.clearValueLock(BasicVisualLexicon.NETWORK_BACKGROUND_PAINT);
			memberNetworkView.clearValueLock(BasicVisualLexicon.NETWORK_TITLE);
			
			for (View<?> view : memberNetworkView.getAllViews()){
				view.clearValueLock(BasicVisualLexicon.NETWORK); //this magically clears all previous locks, but weirdly not for network lexicons
				if (view.getModel() instanceof CyNode){
					view.clearValueLock(BasicVisualLexicon.NODE_WIDTH);
					view.clearValueLock(BasicVisualLexicon.NODE_HEIGHT);
				}
			}
		}
		
		
		//for some reasons, we need to delay applying the visual style to avoid bug
		//possible reason might be that if visual style is applied while layout algorithm is still running, it might not work correctly
		updateViewTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				appAdapter.getVisualMappingManager().setVisualStyle(dynetVisualStyle, unionNetworkView);
				
				for (CyNetworkView memberNetworkView : getMemberNetworkViews()){
					dynetVisualStyle.apply(memberNetworkView); //we need this too because setVisualStyle fails to update certain VisualLexicon instantly
					appAdapter.getVisualMappingManager().setVisualStyle(dynetVisualStyle, memberNetworkView);
					memberNetworkView.updateView();
				}
			}
		}, 1500);
		
		return dynetVisualStyle;
	}
	
	public VisualStyle getVisualStyle(){
		return unionNetworkVisualStyle;
	}
	
	//find rows for which the value in the specified column matches the given parameter
	private Collection<CyRow> getMatchingRows(Collection<CyRow> rows, String columnName, Object value){
		Collection<CyRow> matchingRows = new ArrayList<CyRow>();
		for (CyRow row : rows){
			boolean match;
			Comparable rowValue = (Comparable) row.get(columnName, Object.class);
			
			if (rowValue == null && value == null){
				match = true;
			}else if (rowValue == null || value == null){
				match = false;
			}else if(rowValue.compareTo(value) == 0){
				match = true;
			}else{
				match = false;
			}
				
			if (match) matchingRows.add(row);
		}
		return matchingRows;
	}
	
	
	public List<CustomNode> getCorrespondingNodes(CyNode node, CyNetwork network){
		if (!nodeToId.containsKey(node)) return null;
		long id = nodeToId.get(node);
		List<CustomNode> correspondingNodes = idToNodes.get(id);
		
		List<CustomNode> otherNodesOnly = new ArrayList<CustomNode>();
		for (CustomNode correspondingNode : correspondingNodes){
			if (correspondingNode.getNetwork() != network){
				otherNodesOnly.add(correspondingNode);
			}
		}
		return otherNodesOnly;
	}
	
	public List<CustomEdge> getCorrespondingEdges(CyEdge edge, CyNetwork network){
		if (!edgeToId.containsKey(edge)) return null;
		long id = edgeToId.get(edge);
		List<CustomEdge> correspondingEdges = idToEdges.get(id);
		
		List<CustomEdge> otherEdgesOnly = new ArrayList<CustomEdge>();
		for (CustomEdge correspondingEdge : correspondingEdges){
			if (correspondingEdge.getNetwork() != network){
				otherEdgesOnly.add(correspondingEdge);
			}
		}
		return otherEdgesOnly;
	}
	
	public CyNode getNodeFromRow(CyRow row){
		long suid = row.get(CyNetwork.SUID, Long.class);
		CyNode match = unionNetwork.getNode(suid);
		if (match != null){
			return match;
		}else{
			for (CyNetwork memberNetwork : memberNetworks){
				match = memberNetwork.getNode(suid);
				if (match != null) break;
			}
		}
		return match;
	}
	
	public CyEdge getEdgeFromRow(CyRow row){
		long suid = row.get(CyNetwork.SUID, Long.class);
		CyEdge match = unionNetwork.getEdge(suid);
		if (match != null){
			return match;
		}else{
			for (CyNetwork memberNetwork : memberNetworks){
				match = memberNetwork.getEdge(suid);
				if (match != null) break;
			}
		}
		return match;
	}
	
	
	public List<CyNetworkView> getSyncedNetworkViews(){
		return syncedNetworkViews;
	}
	
	public void addSyncedNetworkViews(List<CyNetworkView> networkViews){
		for(CyNetworkView networkView : networkViews){
			if (!syncedNetworkViews.contains(networkView)){
				syncedNetworkViews.add(networkView);
			}
		}
		
		resyncNetworkViews(networkViews, syncPanZoomEnabled, syncLocationsEnabled, syncSelectionsEnabled);
	}
	
	public void removeSyncedNetworkViews(List<CyNetworkView> networkViews){
		for (CyNetworkView networkView : networkViews){
			syncedNetworkViews.remove(networkView);
		}
	}
	

	
	public boolean getSyncPanZoomEnabled(){
		return syncPanZoomEnabled;
	}
	
	public void setSyncPanZoomEnabled(boolean syncPanZoom){
		syncPanZoomEnabled = syncPanZoom;
		if(syncPanZoomEnabled) resyncNetworkViews(syncedNetworkViews,true, false, false);
	}
	
	
	public boolean getSyncLocationsEnabled(){
		return syncLocationsEnabled;
	}
	
	public void setSyncLocationsEnabled(boolean syncLocations){
		syncLocationsEnabled = syncLocations;
		if (syncLocationsEnabled) resyncNetworkViews(syncedNetworkViews, false, true, false);
	}
	
	public boolean getSyncSelectionsEnabled(){
		return syncSelectionsEnabled;
	}
	
	public void setSyncSelectionsEnabled(boolean syncSelections){
		syncSelectionsEnabled = syncSelections;
		if (syncSelectionsEnabled) resyncNetworkViews(syncedNetworkViews, false, false, true);
	}
	
	public void resyncNetworkViews(List<CyNetworkView> networkViews, boolean syncPanZoom, boolean syncLocations, 
			boolean syncSelections){
		appAdapter.getTaskManager().execute(new TaskIterator(new ResyncNetworkViewsTask(this, networkViews, 
				syncPanZoom, syncLocations, syncSelections)));
	}
	
	
	/**
	 * Handles the synchronization of node and edge selection.
	 * 
	 * Only process events which come from the current active network (meaning that the user made the selection)
	 * otherwise, we'll get caught in an infinite loop.
	 *	
     * this has the down side of, for example, selections won't be synced if it comes from selecting the heatmap when the union network
     * isn't the active network. What would be a good solution?
	 */
	@Override
	public void handleEvent(RowsSetEvent e) {
		
		
		CyNetworkView currentNetworkView = appAdapter.getCyApplicationManager().getCurrentNetworkView();
		if (currentNetworkView != unionNetworkView && !syncedNetworkViews.contains(currentNetworkView))return;
		
		boolean needToUpdate = false;
		
		CyNetwork currentNetwork = currentNetworkView.getModel();
		
		if (e.getSource() == currentNetwork.getDefaultNodeTable()){
			for (RowSetRecord record : e.getPayloadCollection()){
				if (syncSelectionsEnabled && record.getColumn().equals(CyNetwork.SELECTED)){
					CyNode node = currentNetwork.getNode(record.getRow().get(CyNetwork.SUID, Long.class));
					List<CustomNode> otherNodes = getCorrespondingNodes(node, currentNetwork);
					for (CustomNode otherNode : otherNodes){
						CyNetworkView correspondingNetworkView = otherNode.getNetworkView();
						if (syncedNetworkViews.contains(correspondingNetworkView) || correspondingNetworkView == unionNetworkView){
							CyRow otherNodeRow = otherNode.getTableRow();
							if (otherNodeRow != null){
								otherNodeRow.set(CyNetwork.SELECTED, record.getValue());
								needToUpdate = true;
							}
						}
					}
				}
			}
		}else if (e.getSource() == currentNetwork.getDefaultEdgeTable()){
			for (RowSetRecord record : e.getPayloadCollection()){
				if (syncSelectionsEnabled && record.getColumn().equals(CyNetwork.SELECTED)){
					CyEdge edge = currentNetwork.getEdge(record.getRow().get(CyNetwork.SUID, Long.class));
					List<CustomEdge> otherEdges = getCorrespondingEdges(edge, currentNetwork);
					for (CustomEdge otherEdge : otherEdges){
						CyNetworkView correspondingNetworkView = otherEdge.getNetworkView();
						if (syncedNetworkViews.contains(correspondingNetworkView) || correspondingNetworkView == unionNetworkView){
							CyRow otherEdgeRow = otherEdge.getTableRow();
							if (otherEdgeRow != null){
								otherEdgeRow.set(CyNetwork.SELECTED, record.getValue());
								needToUpdate = true;
							}
						}
					}
				}
			}
		}
		
		if (needToUpdate){
			if (currentNetworkView != unionNetworkView) unionNetworkView.updateView();
			for (CyNetworkView networkView : syncedNetworkViews){
				if (currentNetworkView != networkView) networkView.updateView();
			}
		}
	}
	
	/**
	 * Handles the synchronization of node and edges visual lexicons, including node movement and 
	 * node and edge highlighting.
	 * 
	 * For node movement and network drag/zoom, the method only processes events that come from the current active network, meaning that 
	 * the user initiated the event. Otherwise, we'll get caught in an infinite loop. For other events (highlighting),
	 * only events coming from the union network will be processed.
	 */
	@Override
	public void handleEvent(final ViewChangedEvent<?> e) {
		if (e.getSource() != unionNetworkView && !syncedNetworkViews.contains(e.getSource())) return;
		CyNetworkView currentNetworkView = appAdapter.getCyApplicationManager().getCurrentNetworkView();
		CyNetwork currentNetwork = currentNetworkView.getModel();
		
		
		boolean needToUpdate = false;
		
		for (ViewChangeRecord<?> record : e.getPayloadCollection()){
			
			if (record.getView().getModel() instanceof CyNode){
				CyNode node = (CyNode)record.getView().getModel();
				
				if (syncLocationsEnabled && (record.getVisualProperty() == BasicVisualLexicon.NODE_X_LOCATION || 
						record.getVisualProperty() == BasicVisualLexicon.NODE_Y_LOCATION)){
					if (e.getSource() == currentNetworkView){
						for (CustomNode otherNode : getCorrespondingNodes(node, currentNetwork)){
							CyNetworkView correspondingNetworkView = otherNode.getNetworkView();
							if (syncedNetworkViews.contains(correspondingNetworkView) || correspondingNetworkView == unionNetworkView){
								View<CyNode> otherNodeView = otherNode.getNodeView();
								if (otherNodeView != null){
									otherNodeView.setVisualProperty(record.getVisualProperty(), record.getValue());
									needToUpdate = true;
								}
							}
						}
					}
				}
				
			}else if (record.getView() instanceof CyNetworkView){
				
				if (syncPanZoomEnabled && (record.getVisualProperty() == BasicVisualLexicon.NETWORK_CENTER_X_LOCATION ||
						record.getVisualProperty() == BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION ||
						record.getVisualProperty() == BasicVisualLexicon.NETWORK_SCALE_FACTOR)){
					
					if (e.getSource() == currentNetworkView){
						
						if (unionNetworkView != record.getView()){
							unionNetworkView.setVisualProperty(record.getVisualProperty(), record.getValue());
						}
						
						for (CyNetworkView networkView : syncedNetworkViews){
							if (networkView != record.getView()){
								networkView.setVisualProperty(record.getVisualProperty(), record.getValue());		
							}
						}
						
						needToUpdate = true;
					}
				}			
			}
		}
		
		
		if (needToUpdate){
			if (unionNetworkView != e.getSource()) unionNetworkView.updateView();
			for (CyNetworkView networkView : syncedNetworkViews){
				if (networkView != e.getSource()) networkView.updateView();
			}
			
			
			/*The following is a hack to get the networkView to display the edges properly.
			 * The Problems: 
			 * 1. if updateView() is called too soon (< 600ms after modifying the view), sometimes edges won't be shown
			 * 2. updateView() won't have any effect if called again on the same networkView if nothing has changed
			 * The Solutions:
			 * 1. set up a timer to call updateView() after 600ms
			 * 2. before calling updateView() change the X_LOCATION by tiny amount
			 */
			
			if (updateViewTimerTask != null) updateViewTimerTask.cancel();
			updateViewTimerTask = new TimerTask() {
				@Override
				public void run() {
					if (unionNetworkView != e.getSource()){
						unionNetworkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, 
								unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION) + 0.0001);
						unionNetworkView.updateView();
					}
					
					
					for (CyNetworkView networkView : syncedNetworkViews){
						if (networkView != e.getSource()){
							networkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, 
									networkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION) + 0.0001);
							networkView.updateView();
						}
					}
				}
			};
			updateViewTimer.schedule(updateViewTimerTask, 600);
		}
		
	}
	
	public void cleanUp(){
		if (updateViewTimerTask != null) updateViewTimerTask.cancel();
	}
	
	/**
	 * Return columns in the node table, excluding those that are auto generated by cytoscape (such as SUID, SELECTED, and shared name)
	 */
	public List<CyColumn> getNodeColumns(){
		Collection<CyColumn> columns =  unionNetwork.getDefaultNodeTable().getColumns();
		ArrayList<CyColumn> validColumns = new ArrayList<CyColumn>();
		for (CyColumn column : columns){
			String columnName = column.getName();
			if (!columnName.equals(CyNetwork.SUID) && !columnName.equals(CyNetwork.SELECTED) && !columnName.equals("shared name")){
				if (columnName.equals(CyNetwork.NAME) && !nodeAttributeGroups.contains(CyNetwork.NAME)){
					//don't include
					
					//note: if the user doesn't identify corresponding nodes by the NAME attribute, then the NAME attribute in the
					//union network will be empty, because each member network will have its own networkName_NAME column.
				}else{
					validColumns.add(column);
				}
			}
		}
		return validColumns;
	}
	
	/**
	 * return columns in the edge table, excluding those that are auto generated by cytoscape (such as SUID, SELECTED, etc.)
	 */
	public List<CyColumn> getEdgeColumns(){
		Collection<CyColumn> columns = unionNetwork.getDefaultEdgeTable().getColumns();
		ArrayList<CyColumn> validColumns = new ArrayList<CyColumn>();
		for (CyColumn column : columns){
			String columnName = column.getName();
			if (!columnName.equals(CyNetwork.SUID) && !columnName.equals(CyNetwork.SELECTED) &&
				!columnName.equals("shared name") && !columnName.equals("shared interaction")){
				if (columnName.equals(CyNetwork.NAME) && !edgeAttributeGroups.contains(CyNetwork.NAME)){
					//don't include
				}else if (columnName.equals(CyEdge.INTERACTION) && !edgeAttributeGroups.contains(CyEdge.INTERACTION)){
					//don't include
					
					//note: if the user doesn't identify corresponding edges by the NAME or INTERACTION attribute, then 
					//those attributes will be empty in the union network, because each member network will have its own 
					//networkName_NAME or networkName_INTERACTION column.
				}else{
					validColumns.add(column);
				}
			}
		}
		return validColumns;
	}
	
	/**
	 * Returns the list of attributes that were used for identifying corresponding nodes when constructing the dynamic network.
	 */
	public List<String> getGroupedNodeAttributes(){
		return nodeAttributeGroups;
	}
	
	/**
	 * Returns the list of attributes that were used for identifying corresponding edges when constructing the dynamic network.
	 */
	public List<String> getGroupedEdgeAttributes(){
		return edgeAttributeGroups;
	}
	
	/**
	 * Returns the list of attributes that were not used for identifying corresponding nodes, but without the preceding networkName
	 */
	public List<String> getUngroupedNodeAttributes(){
		return new ArrayList<String>(nodeAttributesUngrouped);
	}
	
	/**
	 * Returns the list of attributes that were not used for identifying corresponding edges, but without the preceding networkName
	 */
	public List<String> getUngroupedEdgeAttributes(){
		return new ArrayList<String>(edgeAttributesUngrouped);
	}
	
	
	/**
	 * If the column belongs to a specific network, return the network. Otherwise, if the column is not 
	 * specific to any network (just belongs to the whole dynamic network), return null.
	 */
	public CyNetwork getNetworkOfColumn(CyColumn column){
		return columnToNetworkMap.get(column);
	}
	
	
	
	/*
	 * The main purpose for the existence of the CustomNode and CustomEdge class is because the normal CyNode and CyEdge class
	 * does not store their parent CyNetwork. So while we can use a hashmap to find corresponding nodes and edges, we cannot easily
	 * find which network that corresponding node/edge belongs to.
	 * 
	 * These classes however have been extended to include a number of other convenience method for quickly retrieving their table row,
	 * nodeView/edgeView, etc.
	 */
	
	
	public class CustomNode{
		private CyNode node;
		private CyNetwork network;
		private long dynetID;
		
		public CustomNode(long dynetID, CyNode node, CyNetwork network){
			this.dynetID = dynetID;
			this.node = node;
			this.network = network;
		}
		
		public CyNode getNode(){
			return node;
		}
		
		public CyNetwork getNetwork(){
			if (appAdapter.getCyNetworkManager().networkExists(network.getSUID())){
				return network;
			}else{
				return null;
			}
		}
		
		public CyNetworkView getNetworkView(){
			Collection<CyNetworkView> networkViews = appAdapter.getCyNetworkViewManager().getNetworkViews(network);
			if (networkViews.isEmpty()){
				return null;
			}else{
				return networkViews.iterator().next();
			}
		}
		
		public View<CyNode> getNodeView(){
			CyNetworkView networkView = getNetworkView();
			if (networkView != null) return networkView.getNodeView(node);
			else return null;
		}
		
		public long getDynetId(){
			return dynetID;
		}
		
		public CyRow getTableRow(){
			if (appAdapter.getCyNetworkManager().networkExists(network.getSUID()) && network.containsNode(node)){
				return network.getRow(node);
			}else{
				return null;
			}
		}
	}
	
	public class CustomEdge{
		private CyEdge edge;
		private CyNetwork network;
		private long dynetID;
		
		public CustomEdge(long dynetID, CyEdge edge, CyNetwork network){
			this.dynetID = dynetID;
			this.edge = edge;
			this.network = network;
		}
		
		public CyEdge getEdge(){
			return edge;
		}
		
		public CyNetwork getNetwork(){
			if (appAdapter.getCyNetworkManager().networkExists(network.getSUID())){
				return network;
			}else{
				return null;
			}
		}
		
		public CyNetworkView getNetworkView(){
			Collection<CyNetworkView> networkViews = appAdapter.getCyNetworkViewManager().getNetworkViews(network);
			if (networkViews.isEmpty()){
				return null;
			}else{
				return networkViews.iterator().next();
			}
		}
		
		public View<CyEdge> getEdgeView(){
			CyNetworkView networkView = getNetworkView();
			if (networkView != null) return networkView.getEdgeView(edge);
			else return null;
		}
		
		public long getDynetId(){
			return dynetID;
		}
		
		public CyRow getTableRow(){
			if (appAdapter.getCyNetworkManager().networkExists(network.getSUID()) && network.containsEdge(edge)){
				return network.getRow(edge);
			}else{
				return null;
			}
		}
	}
	
}
