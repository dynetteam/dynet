package eu.primes.dynet.internal.initdialog;

import java.util.List;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynetActivator;

/**
 * This task creates an instance of the DynamicNetwork object and therefore serves as the starting point of the app
 * (after the user clicks OK on the setup dialog).
 * 
 * @author Ivan Hendy Goenawan
 */

public class CreateDynetTask extends AbstractTask{
	private DynetActivator activator;
	private CyAppAdapter appAdapter;
	private List<CyNetwork> memberNetworks;
	private List<String> nodeAttributeGroups;
	private List<String> edgeAttributeGroups;
	private boolean treatAsDirected;
	private CyLayoutAlgorithm defaultLayout;
	
	
	public CreateDynetTask(DynetActivator activator, CyAppAdapter appAdapter, List<CyNetwork> memberNetworks, List<String> nodeAttributeGroups, List<String> edgeAttributeGroups, boolean treatAsDirected, CyLayoutAlgorithm defaultLayout){
		this.activator = activator;
		this.appAdapter = appAdapter;
		this.memberNetworks = memberNetworks;
		this.nodeAttributeGroups = nodeAttributeGroups;
		this.edgeAttributeGroups = edgeAttributeGroups;
		this.treatAsDirected = treatAsDirected;
		this.defaultLayout = defaultLayout;
	}
	
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("DyNet");
		taskMonitor.setStatusMessage("Setting Up DyNet");
		
		DynamicNetwork dynet = new DynamicNetwork(appAdapter, memberNetworks, nodeAttributeGroups, edgeAttributeGroups, treatAsDirected, defaultLayout);
		activator.dynetSetupFinish(dynet);  //notify the activator that the DynamicNetwork object has been created
	}
	
}
