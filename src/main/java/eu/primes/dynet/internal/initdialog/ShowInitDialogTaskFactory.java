
package eu.primes.dynet.internal.initdialog;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.DynetActivator;

/**
 * This is the task factory that is meant to be the first one to be registered to Cytoscape. The resulting task 
 * will show the initial setup dialog.
 * 
 * @author Ivan Hendy Goenawan
 */

public class ShowInitDialogTaskFactory extends AbstractTaskFactory {
	private final CySwingAppAdapter appAdapter;
	private final DynetActivator activator;
	
	public ShowInitDialogTaskFactory(CySwingAppAdapter appAdapter, DynetActivator activator){
		this.appAdapter = appAdapter;
		this.activator = activator;
	}

	@Override
	public TaskIterator createTaskIterator() {
		return new TaskIterator(new ShowInitDialogTask(appAdapter, activator));
	}

	@Override
	public boolean isReady() {
		return super.isReady() && !activator.dynetExists(); //for now we only support one active dynet instance at a time
	}
	
	
}

