package eu.primes.dynet.internal.pairwisecompare;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomNode;

/**
 * This task computes the log fold-change of a node attribute in two networks (one associated with red and the other with
 * green). The resulting log fold-change will be mapped to node colours. 
 * Nodes that have higher values in the 'green' network will have greener colour, and likewise with the 'red' network.
 * If the value is the same in the two networks, then the node colour will stay the same.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeChangeMapperTask extends AbstractTask {
	private PairwiseComparePanel parent;
	private DynamicNetwork dynet;
	private CyNetwork networkA;
	private CyNetwork networkB;
	private String nodeProperty;
	private boolean ignoreAbsence;
	
	private HashMap<CyNode, Double> nodeDifferenceMap;
	private double minDifferenceAboveThreshold;   //in absolute term (no negative)
	private double maxDifferenceBelowThreshold;
	
	
	private HashMap<CyNode, Double> nodeValueMapA;
	private HashMap<CyNode, Double> nodeValueMapB;
	private double minValueAboveThreshold;
	private double maxValueBelowThreshold;
	
	
	private static final Color baseColor = DynamicNetwork.DEFAULT_NODE_COLOR;
	private static final Color networkAColor = new Color(0, 205, 0);
	private static final Color networkBColor = new Color(205, 0, 0);
	
	
	private static final double defaultSize = DynamicNetwork.DEFAULT_NODE_SIZE;
	private static final double minSize = 0.5 * defaultSize;
	private static final double maxSize = 1.5 * defaultSize;
	
	
	public NodeChangeMapperTask(PairwiseComparePanel parent, DynamicNetwork dynet, CyNetwork networkA, CyNetwork networkB, String nodeProperty, boolean ignoreAbsence){
		this.parent = parent;
		this.dynet = dynet;
		this.networkA = networkA;
		this.networkB = networkB;
		this.nodeProperty = nodeProperty;
		this.ignoreAbsence = ignoreAbsence;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Dynet Node Difference Mapper");
		taskMonitor.setStatusMessage("Mapping node changes to node colour.");
		
		calculateNodeDifferences();
		parent.setNodeMapperResult(nodeDifferenceMap);
		findOutlierThresholds();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		for (CyNode node : unionNetwork.getNodeList()){
			
			//first, colour and adjust the size of nodes in the union network based on the fold change
			double difference = nodeDifferenceMap.get(node);
			View<CyNode> nodeView = unionNetworkView.getNodeView(node);
			
			if (difference == Double.POSITIVE_INFINITY){
				if (ignoreAbsence){
					nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, baseColor);
				}else{
					nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkBColor);
				}
				
				if (nodeProperty.equals(DynamicNetwork.PRESENT)){
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, defaultSize);
				}else{
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, maxSize);
				}
			}else if (difference == Double.NEGATIVE_INFINITY){
				if (ignoreAbsence){
					nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, baseColor);
				}else{
					nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkAColor);
				}
				
				if (nodeProperty.equals(DynamicNetwork.PRESENT)){
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, defaultSize);
				}else{
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, maxSize);
				}
			}else if(difference > 0){
				nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkBColor);
				nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, rescale(difference, minDifferenceAboveThreshold, maxDifferenceBelowThreshold, minSize, maxSize));
			}else if (difference < 0){
				nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkAColor);
				nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, rescale(- 1 * difference, minDifferenceAboveThreshold, maxDifferenceBelowThreshold, minSize, maxSize));
			}else{
				nodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, baseColor);
				if (nodeProperty.equals(DynamicNetwork.PRESENT)){
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, defaultSize);
				}else{
					nodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, minSize);
				}
			}
			
			
		
		
			//then colour and adjust size of nodes in networkA and networkB based on their values
			for (CustomNode otherNode : dynet.getCorrespondingNodes(node, unionNetwork)){
				if (otherNode.getNetwork() == networkA){
					View<CyNode> otherNodeView = otherNode.getNodeView();
					if (otherNodeView != null){
						double valueA = nodeValueMapA.get(node);
						if (difference == Double.NEGATIVE_INFINITY){
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkAColor);
						}else{
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, baseColor);
						}
						
						if (nodeProperty.equals(DynamicNetwork.PRESENT)){
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, defaultSize);
						}else{
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, rescale(valueA, minValueAboveThreshold, maxValueBelowThreshold, minSize, maxSize));
						}	
					}
					
				}else if (otherNode.getNetwork() == networkB){
					View<CyNode> otherNodeView = otherNode.getNodeView();
					if (otherNodeView != null){
						double valueB = nodeValueMapB.get(node);
						if (difference == Double.POSITIVE_INFINITY){
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, networkBColor);
						}else{
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_FILL_COLOR, baseColor);
						}
						
						if (nodeProperty.equals(DynamicNetwork.PRESENT)){
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, defaultSize);
						}else{
							otherNodeView.setLockedValue(BasicVisualLexicon.NODE_SIZE, rescale(valueB, minValueAboveThreshold, maxValueBelowThreshold, minSize, maxSize));
						}
					}
				}
			}
		}
		
		unionNetworkView.updateView();
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			if(memberNetworkView.getModel() == networkA || memberNetworkView.getModel() == networkB){
				memberNetworkView.updateView();
			}
		}
	}

	
	private void calculateNodeDifferences() throws Exception{
		nodeDifferenceMap = new HashMap<CyNode, Double>();
		
		nodeValueMapA = new HashMap<CyNode, Double>();
		nodeValueMapB = new HashMap<CyNode, Double>();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		
		String networkAName = networkA.getRow(networkA).get(CyNetwork.NAME, String.class);
		String networkBName = networkB.getRow(networkB).get(CyNetwork.NAME, String.class);
		String columnAName = networkAName + "_" + nodeProperty;
		String columnBName = networkBName + "_" + nodeProperty;
		if (unionNetwork.getDefaultNodeTable().getColumn(columnAName) == null){
			throw new Exception("Node attribute " + nodeProperty + " is not present in " + networkAName + " network.");
		}
		if (unionNetwork.getDefaultNodeTable().getColumn(columnBName) == null){
			throw new Exception("Node attribute " + nodeProperty + " is not present in " + networkBName + " network.");
		}
		
		
		for (CyNode node : unionNetwork.getNodeList()){
			Object valueA = unionNetwork.getRow(node).get(columnAName, Object.class);
			if (valueA == null){
				valueA = new Double(0);
			}else{
				Class<?> attributeAType = unionNetwork.getDefaultNodeTable().getColumn(columnAName).getType();
				if (attributeAType == Boolean.class){
					if ((Boolean)valueA) valueA = new Double(1);
					else valueA = new Double(0);
				}else if (attributeAType == String.class){
					try{
						//try to parse string attributes. Should we just ban string attributes altogether?
						valueA = Double.parseDouble((String)valueA);
					}catch (Exception e){
						valueA = new Double(0);
					}
				}else{
					valueA = new Double(valueA.toString());
				}
			}
			
			if ((Double)valueA < 0){
				throw new Exception("Please choose an attribute containing only positive values.");
			}
			
			
			
			
			
			Object valueB = unionNetwork.getRow(node).get(columnBName, Object.class);
			if (valueB == null){
				valueB = new Double(0);
			}else{
				Class<?> attributeBType = unionNetwork.getDefaultNodeTable().getColumn(columnBName).getType(); 
				if (attributeBType == Boolean.class){
					if ((Boolean)valueB) valueB = new Double(1);
					else valueB = new Double(0);
				}else if (attributeBType == String.class){
					try{
						//try to parse string attributes. Should we just ban string attributes altogether?
						valueB = Double.parseDouble((String)valueB);
					}catch (Exception e){
						valueB = new Double(0);
					}
				}else{
					valueB = new Double(valueB.toString());
				}
			}
			
			if ((Double)valueB < 0){
				throw new Exception("Please choose an attribute containing only positive values.");
			}
			
			
			
			double difference;
			if ((Double)valueA == 0 && (Double)valueB == 0){
				difference = 0;
			}else if ((Double)valueA == 0){
				difference = Double.POSITIVE_INFINITY;
			}else if ((Double)valueB == 0){
				difference = Double.NEGATIVE_INFINITY;
			}else{
				difference = log2((Double)valueB / (Double) valueA);
			}
			
			nodeDifferenceMap.put(node, difference);
			nodeValueMapA.put(node, log2((Double)valueA + 1));  //log(x+1) transformation
			nodeValueMapB.put(node, log2((Double)valueB + 1));  //log(x+1) transformation
		}
	}
	
	
	private void findOutlierThresholds(){
		//find threshold log-fold-change difference excluding outliers
		ArrayList<Double> differenceValues = new ArrayList<Double>();
		for (Double difference : nodeDifferenceMap.values()){
			difference = Math.abs(difference);
			if (difference != 0 && isFinite(difference)){
				differenceValues.add(difference);
			}
		}
		double meanDifference = average(differenceValues);
		double stdDevDifference = stdDev(differenceValues, meanDifference);
		double differenceLowThreshold = meanDifference - (2 * stdDevDifference);
		double differenceHighThreshold = meanDifference + (2 * stdDevDifference);
		
		minDifferenceAboveThreshold = Double.POSITIVE_INFINITY;
		maxDifferenceBelowThreshold = 0;
		for (double difference : differenceValues){
			if (difference < minDifferenceAboveThreshold && difference > differenceLowThreshold){
				minDifferenceAboveThreshold = difference;
			}
			if (difference > maxDifferenceBelowThreshold && difference < differenceHighThreshold){
				maxDifferenceBelowThreshold = difference;
			}
		}
		
		//find threshold value in both networks excluding outliers
		ArrayList<Double> allValues = new ArrayList<Double>();
		for (Double value : nodeValueMapA.values()){
			if (value != 0 && isFinite(value)){
				allValues.add(value);
			}
		}
		for (Double value : nodeValueMapB.values()){
			if (value != 0 && isFinite(value)){
				allValues.add(value);
			}
		}
		double meanValue = average(allValues);
		double stdDevValue = stdDev(allValues, meanValue);
		double valueLowThreshold = meanValue - (2 * stdDevValue);
		double valueHighThreshold = meanValue + (2 * stdDevValue);
		
		minValueAboveThreshold = Double.POSITIVE_INFINITY;
		maxValueBelowThreshold = 0;
		for (double value : allValues){
			if (value < minValueAboveThreshold && value > valueLowThreshold){
				minValueAboveThreshold = value;
			}
			if (value > maxValueBelowThreshold && value < valueHighThreshold){
				maxValueBelowThreshold = value;
			}
		}
	}
	
	
	private static double stdDev(Collection<Double> values, double average){
		double sum = 0;
		
		for (Double value : values){
			sum += (value - average) * (value - average);
		}
		
		if (values.size() == 0) return 0;
		return Math.sqrt(sum / (values.size() - 1));
	}
	
	private static double average(Collection<Double> values){
		double sum = 0;
		
		for (Double value : values){
			sum += value;
		}
		
		if (values.size() == 0) return 0;
		return sum/values.size();
	}
	
	

	private static double rescale(double value, double srcLow, double srcHigh, double destLow, double destHigh){
		if (value < srcLow) return destLow;
		if (value > srcHigh) return destHigh;
		
		double srcInterval = srcHigh - srcLow;
		if (srcInterval == 0) return (destLow + destHigh) / 2;
		
		double fraction = (value - srcLow) / srcInterval;
		return destLow + fraction * (destHigh - destLow);
	}
	
	private static double log2(double value){
		return Math.log(value) / Math.log(2);
	}
	
	
	//because isFinite only exists starting from Java 8
	private static boolean isFinite(double number){
		return !Double.isInfinite(number) && !Double.isNaN(number);
	}
}
