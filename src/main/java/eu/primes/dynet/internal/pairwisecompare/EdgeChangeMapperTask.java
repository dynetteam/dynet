package eu.primes.dynet.internal.pairwisecompare;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomEdge;

/**
 * This task computes the log fold-change of an edge attribute in two networks (one associated with red and the other with
 * green). The resulting log fold-change will be mapped to edge colours. 
 * Edges that have higher values in the 'green' network will have greener colour, and likewise with the 'red' network.
 * If the value is the same in the two networks, then the edge colour will stay the same.
 * 
 * 
 * @author Ivan Hendy Goenawan
 */


public class EdgeChangeMapperTask extends AbstractTask{
	private PairwiseComparePanel parent;
	private DynamicNetwork dynet;
	private CyNetwork networkA;
	private CyNetwork networkB;
	private String edgeProperty;
	private boolean ignoreAbsence;
	
	private HashMap<CyEdge, Double> edgeDifferenceMap;
	private double minDifferenceAboveThreshold;   //in absolute term (no negative)
	private double maxDifferenceBelowThreshold;
	
	private HashMap<CyEdge, Double> edgeValueMapA;
	private HashMap<CyEdge, Double> edgeValueMapB;
	private double minValueAboveThreshold;
	private double maxValueBelowThreshold;
	
	private static final Color baseColor = DynamicNetwork.DEFAULT_EDGE_COLOR;
	private static final Color networkAColor = new Color(0, 205, 0);
	private static final Color networkBColor = new Color(205, 0, 0);
	
	
	private static final double defaultSize = DynamicNetwork.DEFAULT_EDGE_WIDTH;
	private static final double minSize = 1;   //ideally min and max size should be the same distance away from the default
	private static final double maxSize = 5;
	
	
	
	public EdgeChangeMapperTask(PairwiseComparePanel parent, DynamicNetwork dynet, CyNetwork networkA, CyNetwork networkB, String edgeProperty, boolean ignoreAbsence){
		this.parent = parent;
		this.dynet = dynet;
		this.networkA = networkA;
		this.networkB = networkB;
		this.edgeProperty = edgeProperty;
		this.ignoreAbsence = ignoreAbsence;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Dynet Edge Difference Mapper");
		taskMonitor.setStatusMessage("Mapping edge changes to edge colour.");
		
		calculateEdgeDifferences();
		parent.setEdgeMapperResult(edgeDifferenceMap);
		findOutlierThresholds();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		for (CyEdge edge : unionNetwork.getEdgeList()){
			
			//first, colour and adjust the width of edges in the union network based on the fold change
			double difference = edgeDifferenceMap.get(edge);
			View<CyEdge> edgeView = unionNetworkView.getEdgeView(edge);
			
			if (difference == Double.POSITIVE_INFINITY){
				if (ignoreAbsence){
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, baseColor);
				}else{
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkBColor);
				}
				
				if (edgeProperty.equals(DynamicNetwork.PRESENT)){
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, defaultSize);
				}else{
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, maxSize);
				}
			}else if (difference == Double.NEGATIVE_INFINITY){
				if (ignoreAbsence){
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, baseColor);
				}else{
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkAColor);
				}
				
				if (edgeProperty.equals(DynamicNetwork.PRESENT)){
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, defaultSize);
				}else{
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, maxSize);
				}
			}else if(difference > 0){
				edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkBColor);
				edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, rescale(difference, minDifferenceAboveThreshold, maxDifferenceBelowThreshold, minSize, maxSize));
			}else if (difference < 0){
				edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkAColor);
				edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, rescale(-1 * difference, minDifferenceAboveThreshold, maxDifferenceBelowThreshold, minSize, maxSize));
			}else{
				edgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, baseColor);
				if (edgeProperty.equals(DynamicNetwork.PRESENT)){
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, defaultSize);
				}else{
					edgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, minSize);
				}
			}
			
			
			
			//then colour and adjust width of edges in networkA and networkB based on their values
			for (CustomEdge otherEdge : dynet.getCorrespondingEdges(edge, unionNetwork)){
				if (otherEdge.getNetwork() == networkA){
					View<CyEdge> otherEdgeView = otherEdge.getEdgeView();
					if (otherEdgeView != null){
						double valueA = Math.abs(edgeValueMapA.get(edge));
						if (difference == Double.NEGATIVE_INFINITY){
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkAColor);
						}else{
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, baseColor);
						}
						
						if (edgeProperty.equals(DynamicNetwork.PRESENT)){
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, defaultSize);
						}else{
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, rescale(valueA, minValueAboveThreshold, maxValueBelowThreshold, minSize, maxSize));	
						}
					}
					
				}else if (otherEdge.getNetwork() == networkB){
					View<CyEdge> otherEdgeView = otherEdge.getEdgeView();
					if (otherEdgeView != null){
						double valueB = Math.abs(edgeValueMapB.get(edge));
						if (difference == Double.POSITIVE_INFINITY){
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, networkBColor);
						}else{
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, baseColor);
						}
						
						if (edgeProperty.equals(DynamicNetwork.PRESENT)){
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, defaultSize);
						}else{
							otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_WIDTH, rescale(valueB, minValueAboveThreshold, maxValueBelowThreshold, minSize, maxSize));	
						}
					}
				}
			}
			
			
		}
		
		unionNetworkView.updateView();
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			if(memberNetworkView.getModel() == networkA || memberNetworkView.getModel() == networkB){
				memberNetworkView.updateView();
			}
		}
	}

	
	private void calculateEdgeDifferences() throws Exception{
		edgeDifferenceMap = new HashMap<CyEdge, Double>();
		
		edgeValueMapA = new HashMap<CyEdge, Double>();
		edgeValueMapB = new HashMap<CyEdge, Double>();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		
		String networkAName = networkA.getRow(networkA).get(CyNetwork.NAME, String.class);
		String networkBName = networkB.getRow(networkB).get(CyNetwork.NAME, String.class);
		String columnAName = networkAName + "_" + edgeProperty;
		String columnBName = networkBName + "_" + edgeProperty;
		if (unionNetwork.getDefaultEdgeTable().getColumn(columnAName) == null){
			throw new Exception("Edge attribute " + edgeProperty + " is not present in " + networkAName + " network.");
		}
		if (unionNetwork.getDefaultEdgeTable().getColumn(columnBName) == null){
			throw new Exception("Edge attribute " + edgeProperty + " is not present in " + networkBName + " network.");
		}
		
		
		for (CyEdge edge : unionNetwork.getEdgeList()){
			Object valueA = unionNetwork.getRow(edge).get(columnAName, Object.class);
			if (valueA == null){
				valueA = new Double(0);
			}else{
				Class<?> attributeAType = unionNetwork.getDefaultEdgeTable().getColumn(columnAName).getType();
				if (attributeAType == Boolean.class){
					if ((Boolean)valueA) valueA = new Double(1);
					else valueA = new Double(0);
				}else if (attributeAType == String.class){
					try{
						//try to parse string attributes. Should we just ban string attributes altogether?
						valueA = Double.parseDouble((String)valueA);
					}catch (Exception e){
						valueA = new Double(0);
					}
				}else{
					valueA = new Double(valueA.toString());
				}
			}
			
			if ((Double)valueA < 0){
				throw new Exception("Please choose an attribute containing only positive values.");
			}
			
			
			
			Object valueB = unionNetwork.getRow(edge).get(columnBName, Object.class);
			if (valueB == null){
				valueB = new Double(0);
			}else{
				Class<?> attributeBType = unionNetwork.getDefaultEdgeTable().getColumn(columnBName).getType(); 
				if (attributeBType == Boolean.class){
					if ((Boolean)valueB) valueB = new Double(1);
					else valueB = new Double(0);
				}else if (attributeBType == String.class){
					try{
						//try to parse string attributes. Should we just ban string attributes altogether?
						valueB = Double.parseDouble((String)valueB);
					}catch (Exception e){
						valueB = new Double(0);
					}
				}else{
					valueB = new Double(valueB.toString());
				}
			}
			
			if ((Double)valueB < 0){
				throw new Exception("Please choose an attribute containing only positive values.");
			}
			
			
			
			double difference;
			if ((Double)valueA == 0 && (Double)valueB == 0){
				difference = 0;
			}else if ((Double)valueA == 0){
				difference = Double.POSITIVE_INFINITY;
			}else if ((Double)valueB == 0){
				difference = Double.NEGATIVE_INFINITY;
			}else{
				difference = log2((Double)valueB / (Double) valueA);
			}
			
			edgeDifferenceMap.put(edge, difference);
			edgeValueMapA.put(edge, log2((Double)valueA + 1));  //log(x+1) transformation
			edgeValueMapB.put(edge, log2((Double)valueB + 1));  //log(x+1) transformation
		}
	}
	
	
	
	private void findOutlierThresholds(){
		//find threshold log-fold-change difference excluding outliers
		ArrayList<Double> differenceValues = new ArrayList<Double>();
		for (double difference : edgeDifferenceMap.values()){
			difference = Math.abs(difference);
			if (difference != 0 && isFinite(difference)){
				differenceValues.add(difference);
			}
		}
		double meanDifference = average(differenceValues);
		double stdDevDifference = stdDev(differenceValues, meanDifference);
		double differenceLowThreshold = meanDifference - (2 * stdDevDifference);
		double differenceHighThreshold = meanDifference + (2 * stdDevDifference);
		
		minDifferenceAboveThreshold = Double.POSITIVE_INFINITY;
		maxDifferenceBelowThreshold = 0;
		for (double difference : differenceValues){
			if (difference < minDifferenceAboveThreshold && difference > differenceLowThreshold){
				minDifferenceAboveThreshold = difference;
			}
			if (difference > maxDifferenceBelowThreshold && difference < differenceHighThreshold){
				maxDifferenceBelowThreshold = difference;
			}
		}
		
		
		//find threshold value in both networks excluding outliers
		ArrayList<Double> allValues = new ArrayList<Double>();
		for (Double value : edgeValueMapA.values()){
			if (value != 0 && isFinite(value)){
				allValues.add(value);
			}
		}
		for (Double value : edgeValueMapB.values()){
			if (value != 0 && isFinite(value)){
				allValues.add(value);
			}
		}
		double meanValue = average(allValues);
		double stdDevValue = stdDev(allValues, meanValue);
		double valueLowThreshold = meanValue - (2 * stdDevValue);
		double valueHighThreshold = meanValue + (2 * stdDevValue);
		
		minValueAboveThreshold = Double.POSITIVE_INFINITY;
		maxValueBelowThreshold = 0;
		for (double value : allValues){
			if (value < minValueAboveThreshold && value > valueLowThreshold){
				minValueAboveThreshold = value;
			}
			if (value > maxValueBelowThreshold && value < valueHighThreshold){
				maxValueBelowThreshold = value;
			}
		}
	}
	
	
	private static double stdDev(Collection<Double> values, double average){
		double sum = 0;
		
		for (Double value : values){
			sum += (value - average) * (value - average);
		}
		
		if (values.size() == 0) return 0;
		return Math.sqrt(sum / (values.size() - 1));
	}
	
	private static double average(Collection<Double> values){
		double sum = 0;
		
		for (Double value : values){
			sum += value;
		}
		
		if (values.size() == 0) return 0;
		return sum/values.size();
	}
	
	

	private static double rescale(double value, double srcLow, double srcHigh, double destLow, double destHigh){
		if (value < srcLow) return destLow;
		if (value > srcHigh) return destHigh;
		
		double srcInterval = srcHigh - srcLow;
		if (srcInterval == 0) return (destLow + destHigh) / 2;
		
		double fraction = (value - srcLow) / srcInterval;
		return destLow + fraction * (destHigh - destLow);
	}
	
	private static double log2(double value){
		return Math.log(value) / Math.log(2);
	}
	
	
	//because isFinite only exists starting from Java 8
	private static boolean isFinite(double number){
		return !Double.isInfinite(number) && !Double.isNaN(number);
	}
}
