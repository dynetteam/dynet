package eu.primes.dynet.internal.heatmap;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * This class draws the x-axis dendogram on the top side of the heatmap. For now the length of each line does not
 * indicate the distance between the objects. This can be changed in the future.
 * 
 * @author Ivan Hendy Goenawan
 */

public class TopDendogram extends JPanel{

	private TreeNode rootNode;
	private int nodeCount;
	private int maxLevel;
	private int branchLength = 2;
	
	private int width;
	private int height;
	private int colWidth;
	private int currentCol;
	private Graphics g;
	
	public TopDendogram(TreeNode rootNode, int nodeCount, int maxLevel){
		this.rootNode = rootNode;
		this.nodeCount = nodeCount;
		this.maxLevel = maxLevel;
		
		this.setPreferredSize(new Dimension(0, (maxLevel + 1) * branchLength));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.g = g;
		this.width = getWidth();
		this.height = getHeight();
		this.colWidth = width / nodeCount;
		
		currentCol = 0;
		printNode(rootNode);
	}
	
	//Draws a dendogram TreeNode in a recursive manner. Tree is drawn bottom up from the leaf first.
	private Point printNode(TreeNode node){
		if (node.isLeaf()){
			int x = (currentCol * colWidth) + (int)Math.round((double)colWidth / 2);
			g.drawLine(x, height - branchLength, x, height);     //draw the vertical branch line
			currentCol++;
			return new Point(x, height - branchLength);
		}else{
			int minX = width;
			int maxX = 0;
			int minY = height;
			
			ArrayList<Point> childNodePositions = new ArrayList<Point>();
			for (TreeNode child : node.getChildren()){
				Point endPosition = printNode(child);
				childNodePositions.add(endPosition);
				
				if (endPosition.x < minX) minX = endPosition.x;
				if (endPosition.x > maxX) maxX = endPosition.x;
				if (endPosition.y < minY) minY = endPosition.y;
			}
			
			g.drawLine(minX, minY, maxX, minY);    //connect the children with a horizontal line
			
			//extends children that are not as long as their siblings
			for (Point endPosition : childNodePositions){
				g.drawLine(endPosition.x, minY, endPosition.x, endPosition.y);
			}
			
			int midX = minX + (int)Math.round((double)(maxX-minX) / 2);
			g.drawLine(midX, minY - branchLength, midX, minY);  //draw the vertical branch line
			return new Point(midX, minY - branchLength);
		}
	}
}
