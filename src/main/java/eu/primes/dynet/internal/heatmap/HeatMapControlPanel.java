package eu.primes.dynet.internal.heatmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.events.RowSetRecord;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.ControlPanel.ControlPanelComponent;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This is the ControlPanelComponent that shows the menus to create a heatmap.
 * 
 * 
 * @author Ivan Hendy Goenawan
 */

public class HeatMapControlPanel extends ControlPanelComponent implements RowsSetListener {
	private ControlPanel controlPanel;
	private DynamicNetwork dynet;
	private CySwingAppAdapter appAdapter;
	
	private JScrollPane networkListScrollPane;
	private JList<CyNetwork> networkList;
	private JComboBox<String> edgePropertyComboBox;
	private JButton generateButton;
	private JLabel chooseNetworksLabel;
	private JLabel edgePropertyLabel;
	private JRadioButton clusterEdgeValuesButton;
	private JRadioButton clusterEdgeLocationButton;
	private JLabel clusterEdgesLabel;
	private JCheckBox clusterNetworksCheckBox;
	
	private HeatMapFrame heatMapFrame;
	
	private String edgeProperty;
	private List<CyNetwork> selectedNetworks;
	private boolean clusterEdgeValues;      //if true, cluster based on edge values, otherwise cluster based on coordinate
	private boolean clusterNetworks;
	

	public HeatMapControlPanel (final ControlPanel controlPanel, final DynamicNetwork dynet, final CySwingAppAdapter appAdapter){
		this.controlPanel = controlPanel;
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		
		
		List<String> ungroupedEdgeAttributes = dynet.getUngroupedEdgeAttributes();
		Collections.sort(ungroupedEdgeAttributes);
		edgeProperty = DynamicNetwork.PRESENT;
		edgePropertyComboBox = new JComboBox<String>(ungroupedEdgeAttributes.toArray(new String[0]));
		edgePropertyComboBox.setSelectedIndex(ungroupedEdgeAttributes.indexOf(DynamicNetwork.PRESENT));
		edgePropertyComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					edgeProperty = (String)e.getItem();
				}
			}
		});
		
		
		networkList = new JList<CyNetwork>(){
			@Override
			public boolean getScrollableTracksViewportHeight() {
				return false;
			}
		};
		DefaultListModel<CyNetwork> listModel = new DefaultListModel<CyNetwork>();
		for (CyNetwork network : dynet.getMemberNetworks()){
			listModel.addElement(network);
		}
		networkList.setModel(listModel);
		networkList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		networkList.setSelectionInterval(0, dynet.getMemberNetworks().size() - 1);
		selectedNetworks = dynet.getMemberNetworks();
		networkList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false){
					selectedNetworks = ((JList<CyNetwork>)e.getSource()).getSelectedValuesList();
				}
			}
		});
		
		//to enable list reordering
		MouseAdapter reorderListener = new MouseAdapter() {
			private int pressIndex = 0;
			private int releaseIndex = 0;
			
			@Override
			public void mousePressed(MouseEvent e) {
				pressIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
			}
	
			@Override
			public void mouseReleased(MouseEvent e) {
				releaseIndex = ((JList)e.getSource()).locationToIndex(e.getPoint());
				if (releaseIndex != pressIndex && releaseIndex != -1) {
					DefaultListModel model = (DefaultListModel) ((JList)e.getSource()).getModel();
					Object item = model.elementAt(pressIndex);
					model.removeElementAt(pressIndex);
					model.insertElementAt(item, releaseIndex);
				}
			}
	
			@Override
			public void mouseDragged(MouseEvent e) {
				mouseReleased(e);
				pressIndex = releaseIndex;
			}
		};
		networkList.addMouseListener(reorderListener);
		networkList.addMouseMotionListener(reorderListener);
		
		
		clusterEdgeValuesButton = new JRadioButton("weight values");
		clusterEdgeValuesButton.setSelected(true);
		clusterEdgeValues = true;
		clusterEdgeValuesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clusterEdgeValues = true;
			}
		});
		clusterEdgeLocationButton = new JRadioButton("spatial proximity");
		clusterEdgeLocationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clusterEdgeValues = false;
			}
		});
		ButtonGroup group = new ButtonGroup();
		group.add(clusterEdgeLocationButton);
		group.add(clusterEdgeValuesButton);
		
		
		
		clusterNetworksCheckBox = new JCheckBox("Cluster networks");
		clusterNetworks = true;
		clusterNetworksCheckBox.setSelected(true);
		clusterNetworksCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					clusterNetworks = true;
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					clusterNetworks = false;
				}
			}
		});
		
		
		generateButton = new JButton("Generate");
		generateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				appAdapter.getTaskManager().execute(new TaskIterator(
						new GenerateHeatMapTask(HeatMapControlPanel.this, appAdapter, dynet, selectedNetworks, edgeProperty, clusterEdgeValues, clusterNetworks)));
			}
		});
		
		
		
		setBorder(new TitledBorder(new LineBorder(Color.BLACK), "Heatmap", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 150, 10, 150, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 40, 0, 0, 40, 40, 40, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		chooseNetworksLabel = new JLabel("Choose included networks:");
		GridBagConstraints gbc_chooseNetworksLabel = new GridBagConstraints();
		gbc_chooseNetworksLabel.anchor = GridBagConstraints.WEST;
		gbc_chooseNetworksLabel.insets = new Insets(0, 0, 5, 5);
		gbc_chooseNetworksLabel.gridx = 1;
		gbc_chooseNetworksLabel.gridy = 1;
		add(chooseNetworksLabel, gbc_chooseNetworksLabel);
		
		networkListScrollPane = new JScrollPane();
		networkListScrollPane.getViewport().setBackground(Color.WHITE);
		GridBagConstraints gbc_networkListScrollPane = new GridBagConstraints();
		gbc_networkListScrollPane.gridheight = 7;
		gbc_networkListScrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_networkListScrollPane.fill = GridBagConstraints.BOTH;
		gbc_networkListScrollPane.gridx = 1;
		gbc_networkListScrollPane.gridy = 2;
		add(networkListScrollPane, gbc_networkListScrollPane);
		networkListScrollPane.setViewportView(networkList);
		
		edgePropertyLabel = new JLabel("Edge weight property:");
		GridBagConstraints gbc_edgePropertyLabel = new GridBagConstraints();
		gbc_edgePropertyLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgePropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_edgePropertyLabel.gridx = 3;
		gbc_edgePropertyLabel.gridy = 2;
		add(edgePropertyLabel, gbc_edgePropertyLabel);
		
		GridBagConstraints gbc_edgePropertyComboBox = new GridBagConstraints();
		gbc_edgePropertyComboBox.anchor = GridBagConstraints.NORTH;
		gbc_edgePropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_edgePropertyComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_edgePropertyComboBox.gridx = 3;
		gbc_edgePropertyComboBox.gridy = 3;
		add(edgePropertyComboBox, gbc_edgePropertyComboBox);
		
		clusterEdgesLabel = new JLabel("Cluster edges based on:");
		GridBagConstraints gbc_clusterEdgesLabel = new GridBagConstraints();
		gbc_clusterEdgesLabel.anchor = GridBagConstraints.WEST;
		gbc_clusterEdgesLabel.insets = new Insets(0, 0, 5, 5);
		gbc_clusterEdgesLabel.gridx = 3;
		gbc_clusterEdgesLabel.gridy = 4;
		add(clusterEdgesLabel, gbc_clusterEdgesLabel);
		
		
		GridBagConstraints gbc_clusterEdgeValuesButton = new GridBagConstraints();
		gbc_clusterEdgeValuesButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_clusterEdgeValuesButton.insets = new Insets(0, 0, 5, 5);
		gbc_clusterEdgeValuesButton.gridx = 3;
		gbc_clusterEdgeValuesButton.gridy = 5;
		add(clusterEdgeValuesButton, gbc_clusterEdgeValuesButton);
		
		
		GridBagConstraints gbc_clusterEdgeLocationButton = new GridBagConstraints();
		gbc_clusterEdgeLocationButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_clusterEdgeLocationButton.insets = new Insets(0, 0, 5, 5);
		gbc_clusterEdgeLocationButton.gridx = 3;
		gbc_clusterEdgeLocationButton.gridy = 6;
		add(clusterEdgeLocationButton, gbc_clusterEdgeLocationButton);
		
		
		GridBagConstraints gbc_clusterNetworksCheckBox = new GridBagConstraints();
		gbc_clusterNetworksCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_clusterNetworksCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_clusterNetworksCheckBox.gridx = 3;
		gbc_clusterNetworksCheckBox.gridy = 7;
		add(clusterNetworksCheckBox, gbc_clusterNetworksCheckBox);
		
		
		GridBagConstraints gbc_generateButton = new GridBagConstraints();
		gbc_generateButton.anchor = GridBagConstraints.WEST;
		gbc_generateButton.insets = new Insets(0, 0, 5, 5);
		gbc_generateButton.gridx = 3;
		gbc_generateButton.gridy = 8;
		add(generateButton, gbc_generateButton);
	}
	

	public void setHeatMapFrame(HeatMapFrame newHeatMapFrame){
		if (heatMapFrame != null){
			heatMapFrame.dispose();
		}
		heatMapFrame = newHeatMapFrame;
		
		Dimension current = newHeatMapFrame.getPreferredSize();
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int maxWidth = (int) (0.9 * screen.width);
		int maxHeight = (int) (0.9 * screen.height);
		
		int newWidth = current.width > maxWidth ? maxWidth : current.width + 50;
		int newHeight = current.height > maxHeight ? maxHeight : current.height;
		newHeatMapFrame.setPreferredSize(new Dimension(newWidth, newHeight));
		
		newHeatMapFrame.pack();
		newHeatMapFrame.setLocationRelativeTo(appAdapter.getCySwingApplication().getJFrame());
		newHeatMapFrame.setVisible(true);
	}
	
	
	//when user selects an edge on the network, select the corresponding row on the heatmap
	@Override
	public void handleEvent(RowsSetEvent e) {
		if (heatMapFrame != null && e.getSource() == dynet.getUnionNetwork().getDefaultEdgeTable()){
			for (RowSetRecord record : e.getPayloadCollection()){
				CyEdge edge = dynet.getUnionNetwork().getEdge(record.getRow().get(CyNetwork.SUID, Long.class));
				heatMapFrame.setEdgeSelection(edge, (Boolean)record.getValue());
			}
		}
	}

	@Override
	public void update() {
		//do nothing. This component doesn't directly control the appearance of networks.
	}
}
