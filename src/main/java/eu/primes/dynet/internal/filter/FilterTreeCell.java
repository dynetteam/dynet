package eu.primes.dynet.internal.filter;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeModel;

import org.cytoscape.model.CyColumn;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.filter.FilterNode.JoinType;

/**
 * This is the user interface for each node in the FilterTree except the root node which has a slightly different interface.
 * If a node/edge attribute has been selected, the appropriate CyRowFilterPanel will be displayed depending on the type of the attribute
 * (one of String, Boolean, or Numeric).
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterTreeCell extends JPanel {
	
	private FilterNode filterNode;
	private DynamicNetwork dynet;
	private List<CyColumn> nodeAttributes;
	private List<CyColumn> edgeAttributes;
	private List<String> ungroupedNodeAttributes;
	private List<String> ungroupedEdgeAttributes;
	private JTree filterTree;
	
	private JButton deleteButton;
	private JComboBox<Object> selectionComboBox;
	private JButton addChildButton;
	private CyRowFilterPanel rowFilterPanel;
	
	private ActionListener listener;
	
	
	public FilterTreeCell(FilterNode filterNode, DynamicNetwork dynet, JTree filterTree, final ActionListener listener){
		
		this.filterNode = filterNode;
		this.nodeAttributes = dynet.getNodeColumns();
		this.edgeAttributes = dynet.getEdgeColumns();
		this.ungroupedNodeAttributes = dynet.getUngroupedNodeAttributes();
		this.ungroupedEdgeAttributes = dynet.getUngroupedEdgeAttributes();
		this.filterTree = filterTree;
		this.listener = listener;
		
		
		//combobox to select either a JoinType (OR, AND, XOR, NOT) or a node/edge attribute
		selectionComboBox = new JComboBox<Object>(){
			@Override
			public void setSelectedIndex(int index) {
				Object item = getItemAt(index);
				if (!FilterTreeCell.this.filterNode.isLeaf()){
					if (item instanceof JoinType){
						if (!(item == JoinType.NOT && FilterTreeCell.this.filterNode.getChildCount() > 1)){
							//if the node already has children, only allow JoinType (cannot choose a node/edge attribute)
							//also if there are more than 1 child, NOT will not be allowed
							super.setSelectedIndex(index);
						}
					}
				}else{
					super.setSelectedIndex(index);
				}
			}
		};
		
		selectionComboBox.addItem("Choose column or create group...");
		selectionComboBox.addItem(FilterNode.JoinType.AND);
		selectionComboBox.addItem(FilterNode.JoinType.OR);
		selectionComboBox.addItem(FilterNode.JoinType.XOR);
		selectionComboBox.addItem(FilterNode.JoinType.NOT);
		for (String attributeName : ungroupedNodeAttributes){
			ColumnGroup columnGroup = null;
			try{
				columnGroup = new ColumnGroup(dynet, attributeName, true, false);
			}catch (Exception e){
				//Meaning attribute not present in all networks or not all columns have the same type
			}
			if (columnGroup != null) selectionComboBox.addItem(columnGroup);
		}
		for (String attributeName : ungroupedNodeAttributes){
			ColumnGroup columnGroup = null;
			try{
				columnGroup = new ColumnGroup(dynet, attributeName, true, true);
			}catch (Exception e){
				//Meaning attribute not present in all networks or not all columns have the same type
			}
			if (columnGroup != null) selectionComboBox.addItem(columnGroup);
		}
		for (CyColumn column : nodeAttributes){
			selectionComboBox.addItem(new ColumnGroup(dynet, column, true));
		}
		for (String attributeName : ungroupedEdgeAttributes){
			ColumnGroup columnGroup = null;
			try{
				columnGroup = new ColumnGroup(dynet, attributeName, false, false);
			}catch (Exception e){
				//Meaning attribute not present in all networks or not all columns have the same type
			}
			if (columnGroup != null) selectionComboBox.addItem(columnGroup);
		}
		for (String attributeName : ungroupedEdgeAttributes){
			ColumnGroup columnGroup = null;
			try{
				columnGroup = new ColumnGroup(dynet, attributeName, false, true);
			}catch (Exception e){
				//Meaning attribute not present in all networks or not all columns have the same type
			}
			if (columnGroup != null) selectionComboBox.addItem(columnGroup);
		}
		for(CyColumn column : edgeAttributes){
			selectionComboBox.addItem(new ColumnGroup(dynet, column, false));
		}
		
		selectionComboBox.setRenderer(new DefaultListCellRenderer(){
			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				
				boolean disabled = false;
				
				//if the node already has children, disable the options other than JoinType (cannot choose a node/edge attribute)
				//also disallow NOT if the child is more than 1
				if (!FilterTreeCell.this.filterNode.isLeaf()){
					if (!(value instanceof JoinType)){
						disabled = true;
					}else if (value == JoinType.NOT && FilterTreeCell.this.filterNode.getChildCount() > 1){
						disabled = true;
					}
				}
				
				Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				
				if (disabled){
					c.setForeground(UIManager.getColor("Label.disabledForeground"));
				}
				
				return c;
			}
			
		});
		
		if (filterNode.getJoinType() != null){
			selectionComboBox.setSelectedItem(filterNode.getJoinType());
		}else if (filterNode.getRowFilter() != null){
			//find matching ColumnGroup
			ColumnGroup filterColumnGroup = filterNode.getRowFilter().getColumnGroup();
			for (int i = 0; i < selectionComboBox.getItemCount(); i++){
				if (filterColumnGroup.equals(selectionComboBox.getItemAt(i))){
					selectionComboBox.setSelectedIndex(i);
					break;
				}
			}
			
			if (selectionComboBox.getSelectedIndex() == 0){
				//if index is still 0, it means that the item is not in the list, meaning that the
				//attribute may have been removed, so just reset the filter.
				filterNode.setJoinType(null);
				filterNode.setRowFilter(null);

				if (listener != null){
					listener.actionPerformed(new ActionEvent(FilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		}
		
		selectionComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Object value = e.getItem();
					if (value instanceof String){
						FilterTreeCell.this.filterNode.setJoinType(null);
						FilterTreeCell.this.filterNode.setRowFilter(null);
					}else if(value instanceof ColumnGroup){
						FilterTreeCell.this.filterNode.setJoinType(null);
						FilterTreeCell.this.filterNode.setRowFilter(CyRowFilter.createRowFilter((ColumnGroup)value));
					}else if (value instanceof FilterNode.JoinType){
						FilterTreeCell.this.filterNode.setJoinType((FilterNode.JoinType)value);
						FilterTreeCell.this.filterNode.setRowFilter(null);
					}
					
					((DefaultTreeModel)FilterTreeCell.this.filterTree.getModel()).reload(FilterTreeCell.this.filterNode);
					FilterTreeCell.this.filterTree.stopEditing();
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(FilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
					}
				}
			}
		});
		
		
		//Button to delete this node and all its children
		deleteButton = new JButton("X");
		deleteButton.setMargin(new Insets(1, 1, 1, 1));
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FilterNode parent = (FilterNode) FilterTreeCell.this.filterNode.getParent();
				FilterTreeCell.this.filterNode.removeFromParent();
				((DefaultTreeModel)FilterTreeCell.this.filterTree.getModel()).reload(FilterTreeCell.this.filterNode);
				((DefaultTreeModel)FilterTreeCell.this.filterTree.getModel()).reload(parent);
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(FilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		//Button to add a child. Only available when the node is set to contain a JoinType and not a CyRowFilter. 
		addChildButton = new JButton("+");
		addChildButton.setMargin(new Insets(1, 1, 1, 1));
		if (filterNode.getJoinType() == null){
			addChildButton.setVisible(false);
		}else if (filterNode.getJoinType() == JoinType.NOT && filterNode.getChildCount() != 0){
			addChildButton.setEnabled(false);
		}
		
		addChildButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FilterTreeCell.this.filterNode.add(new FilterNode());
				((DefaultTreeModel)FilterTreeCell.this.filterTree.getModel()).reload(FilterTreeCell.this.filterNode);
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(FilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		setBorder(new EmptyBorder(4, 4, 4, 4));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{30, 0, 30, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		GridBagConstraints gbc_deleteButton = new GridBagConstraints();
		gbc_deleteButton.fill = GridBagConstraints.BOTH;
		gbc_deleteButton.insets = new Insets(0, 0, 5, 5);
		gbc_deleteButton.gridx = 0;
		gbc_deleteButton.gridy = 0;
		add(deleteButton, gbc_deleteButton);
		
		GridBagConstraints gbc_selectionComboBox = new GridBagConstraints();
		gbc_selectionComboBox.fill = GridBagConstraints.BOTH;
		gbc_selectionComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_selectionComboBox.gridx = 1;
		gbc_selectionComboBox.gridy = 0;
		add(selectionComboBox, gbc_selectionComboBox);
		
		
		GridBagConstraints gbc_addChildButton = new GridBagConstraints();
		gbc_addChildButton.insets = new Insets(0, 0, 5, 0);
		gbc_addChildButton.fill = GridBagConstraints.BOTH;
		gbc_addChildButton.gridx = 2;
		gbc_addChildButton.gridy = 0;
		add(addChildButton, gbc_addChildButton);
		
		
		if (filterNode.getRowFilter() != null){
			rowFilterPanel = CyRowFilterPanel.createCyRowFilterPanel(filterNode.getRowFilter(), listener);
			
			GridBagConstraints gbc_rowFilterPanel = new GridBagConstraints();
			gbc_rowFilterPanel.fill = GridBagConstraints.HORIZONTAL;
			gbc_rowFilterPanel.insets = new Insets(0, 0, 0, 5);
			gbc_rowFilterPanel.gridx = 1;
			gbc_rowFilterPanel.gridy = 1;
			add(rowFilterPanel, gbc_rowFilterPanel);
		}
		
	}
}
