package eu.primes.dynet.internal.filter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * The user interface for StringCyRowFilter. It consist of a combobox to choose the type of matching (CONTAINS, NOT_CONTAINS, EQUALS, NOT_EQUALS),
 * a checkbox for setting the case sensitivity, and a textbox for typing in the query.
 * 
 * @author Ivan Hendy Goenawan
 */
public class StringCyRowFilterPanel extends CyRowFilterPanel {
	
	private StringCyRowFilter rowFilter;
	private ActionListener listener;
	private JComboBox<StringCyRowFilter.FilterType> filterTypeComboBox;
	private JTextField valueTextField;
	private JCheckBox caseSensitiveCheckBox;
	
	private boolean needToUpdate = false;

	public StringCyRowFilterPanel(StringCyRowFilter rowFilter, final ActionListener listener) {
		super(rowFilter, listener);
		this.rowFilter = rowFilter;
		this.listener = listener;
		
		
		filterTypeComboBox = new JComboBox<StringCyRowFilter.FilterType>(StringCyRowFilter.FilterType.values());
		if (rowFilter.getFilterType() == null){
			rowFilter.setFilterType(StringCyRowFilter.FilterType.CONTAINS);
			needToUpdate = true;
		}
		filterTypeComboBox.setSelectedItem(rowFilter.getFilterType());
		filterTypeComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Object value = e.getItem();
					StringCyRowFilterPanel.this.rowFilter.setFilterType((StringCyRowFilter.FilterType)value);
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(StringCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
					}
				}
			}
		});
		
		
		
		valueTextField = new JTextField();
		if (rowFilter.getValue() == null){
			rowFilter.setValue("");
			needToUpdate = true;
		}
		valueTextField.setText(rowFilter.getValue().toString());
		valueTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {	
			}
			@Override
			public void focusGained(FocusEvent e) {
				valueTextField.selectAll();
			}
		});
		valueTextField.getDocument().addDocumentListener(new DocumentListener() { //use document listener so that filter is updated every keystroke
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			public void update(){
				StringCyRowFilterPanel.this.rowFilter.setValue(valueTextField.getText());
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(StringCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		caseSensitiveCheckBox = new JCheckBox("Case sensitive");
		if (rowFilter.getCaseSensitive()) caseSensitiveCheckBox.setSelected(true);
		caseSensitiveCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					StringCyRowFilterPanel.this.rowFilter.setCaseSensitive(true);
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					StringCyRowFilterPanel.this.rowFilter.setCaseSensitive(false);
				}
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(StringCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		if (needToUpdate && listener != null){
			listener.actionPerformed(new ActionEvent(StringCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
		}
		
		
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		GridBagConstraints gbc_filterTypeComboBox = new GridBagConstraints();
		gbc_filterTypeComboBox.fill = GridBagConstraints.VERTICAL;
		gbc_filterTypeComboBox.anchor = GridBagConstraints.WEST;
		gbc_filterTypeComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_filterTypeComboBox.gridx = 0;
		gbc_filterTypeComboBox.gridy = 0;
		add(filterTypeComboBox, gbc_filterTypeComboBox);
		
		
		GridBagConstraints gbc_valueTextField = new GridBagConstraints();
		gbc_valueTextField.insets = new Insets(0, 0, 5, 0);
		gbc_valueTextField.fill = GridBagConstraints.BOTH;
		gbc_valueTextField.gridx = 1;
		gbc_valueTextField.gridy = 0;
		add(valueTextField, gbc_valueTextField);
		valueTextField.setColumns(10);
		
		
		GridBagConstraints gbc_caseSensitiveCheckBox = new GridBagConstraints();
		gbc_caseSensitiveCheckBox.anchor = GridBagConstraints.WEST;
		gbc_caseSensitiveCheckBox.gridwidth = 2;
		gbc_caseSensitiveCheckBox.fill = GridBagConstraints.VERTICAL;
		gbc_caseSensitiveCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_caseSensitiveCheckBox.gridx = 0;
		gbc_caseSensitiveCheckBox.gridy = 1;
		add(caseSensitiveCheckBox, gbc_caseSensitiveCheckBox);
		
	}

}
