package eu.primes.dynet.internal.filter;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * The renderer for each node in the FilterTree. Returns either a RootFilterTreeCell or a FilterTreeCell depending on
 * whether or not the node is the root node.
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterTreeCellRenderer implements TreeCellRenderer{

	private JTree filterTree;
	private DynamicNetwork dynet;
	private ActionListener listener;
	
	public FilterTreeCellRenderer(JTree filterTree, DynamicNetwork dynet, ActionListener listener){
		this.filterTree = filterTree;
		this.dynet = dynet;
		this.listener = listener;
	}
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		
		if (row == 0) return new RootFilterTreeCell((FilterNode)value, filterTree, listener);
		else return new FilterTreeCell((FilterNode) value, dynet, filterTree, listener);
	}

}
