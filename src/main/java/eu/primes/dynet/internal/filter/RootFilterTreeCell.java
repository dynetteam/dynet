package eu.primes.dynet.internal.filter;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeModel;

import eu.primes.dynet.internal.filter.FilterNode.JoinType;

/**
 * This is the user interface for the root node in the FilterTree. The root is different in that it cannot be deleted,
 * users can only choose a JoinType (not a node/edge attribute) and it has an extra button for adding a parent, moving the
 * whole tree down one level.
 * 
 * @author Ivan Hendy Goenawan
 */
public class RootFilterTreeCell extends JPanel {
	private FilterNode rootNode;
	private JTree filterTree;
	private ActionListener listener;
	private JComboBox<JoinType> selectionComboBox;
	private JButton addChildButton;
	private JButton addParentButton;
	
	
	
	public RootFilterTreeCell(FilterNode rootNode, JTree filterTree, final ActionListener listener){
		
		this.rootNode = rootNode;
		this.filterTree = filterTree;
		this.listener = listener;
		
		selectionComboBox = new JComboBox<JoinType>(JoinType.values()){
			@Override
			public void setSelectedIndex(int index) {
				Object item = getItemAt(index);
				if (!(item == JoinType.NOT && RootFilterTreeCell.this.rootNode.getChildCount() > 1)){
					super.setSelectedIndex(index);
				}
			}
		};
		if (rootNode.getJoinType() == null) rootNode.setJoinType(JoinType.AND);
		selectionComboBox.setSelectedItem(rootNode.getJoinType());
		selectionComboBox.setRenderer(new DefaultListCellRenderer(){
			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value == JoinType.NOT && RootFilterTreeCell.this.rootNode.getChildCount() > 1){
					c.setForeground(UIManager.getColor("Label.disabledForeground"));
				}
				return c;
			}
		});
		selectionComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					JoinType value = (JoinType)e.getItem();
					RootFilterTreeCell.this.rootNode.setJoinType(value);
					
					((DefaultTreeModel)RootFilterTreeCell.this.filterTree.getModel()).reload(RootFilterTreeCell.this.rootNode);
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(RootFilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
					}
				}
			}
		});
		
		
		
		addChildButton = new JButton("+");
		addChildButton.setMargin(new Insets(1, 1, 1, 1));
		if (rootNode.getJoinType() == JoinType.NOT && rootNode.getChildCount() != 0){
			addChildButton.setEnabled(false);
		}
		addChildButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RootFilterTreeCell.this.rootNode.add(new FilterNode());
				((DefaultTreeModel)RootFilterTreeCell.this.filterTree.getModel()).reload(RootFilterTreeCell.this.rootNode);
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(RootFilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		addParentButton = new JButton("^");
		addParentButton.setMargin(new Insets(1, 1, 1, 1));
		if (rootNode.isLeaf()) addParentButton.setEnabled(false);
		addParentButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FilterNode newRoot = new FilterNode();
				newRoot.setJoinType(JoinType.AND);
				newRoot.add(RootFilterTreeCell.this.rootNode);
				
				((DefaultTreeModel)RootFilterTreeCell.this.filterTree.getModel()).setRoot(newRoot);
				((DefaultTreeModel)RootFilterTreeCell.this.filterTree.getModel()).reload();
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(RootFilterTreeCell.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		
		setBorder(new EmptyBorder(2, 2, 2, 2));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{30, 0, 30, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		GridBagConstraints gbc_addParentButton = new GridBagConstraints();
		gbc_addParentButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_addParentButton.insets = new Insets(0, 0, 0, 5);
		gbc_addParentButton.gridx = 0;
		gbc_addParentButton.gridy = 0;
		add(addParentButton, gbc_addParentButton);
		
		
		GridBagConstraints gbc_selectionComboBox = new GridBagConstraints();
		gbc_selectionComboBox.anchor = GridBagConstraints.WEST;
		gbc_selectionComboBox.insets = new Insets(0, 0, 0, 5);
		gbc_selectionComboBox.gridx = 1;
		gbc_selectionComboBox.gridy = 0;
		add(selectionComboBox, gbc_selectionComboBox);
		
		
		GridBagConstraints gbc_addButton = new GridBagConstraints();
		gbc_addButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_addButton.gridx = 2;
		gbc_addButton.gridy = 0;
		add(addChildButton, gbc_addButton);
	}
	
	
}
