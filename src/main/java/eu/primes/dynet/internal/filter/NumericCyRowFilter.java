package eu.primes.dynet.internal.filter;

import java.util.Set;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

/**
 * A filter that can evaluate a CyRow based on a range of numeric attribute (one of Integer, Long, or Double)
 * 
 * @author Ivan Hendy Goenawan
 */
public class NumericCyRowFilter extends CyRowFilter{
	
	private Object lowValue;
	private Object highValue;
	private boolean inclusive = true;
	
	
	public NumericCyRowFilter(ColumnGroup columnGroup) {
		super(columnGroup);
	}
	
	/**
	 * Evaluates whether or not the row matches the filtering criteria (its values in the specified columns are within the allowed range)
	 * 
	 * If no column has been specified, return null for undefined result.
	 * If column's table doesn't match the evaluated row's table (such as when an edge filter is used to evaluate a node), also return null.
	 * 
	 * If network is not given (null), the filter should evaluate all columns in the columngroup.
	 * Otherwise only evaluate columns specific to the network + columns not specific to any network.
	 */
	@Override
	public Boolean evaluate(CyRow row, CyNetwork network){
		if (columnGroup == null) return null;
		if (columnGroup.getTable() != row.getTable()) return null;
		if (lowValue == null || highValue == null) return false;
		
		Boolean result = null;
		for (CyColumn column : columnGroup.getColumns(network)){
			Object rowValue = row.get(column.getName(), column.getType());
			
			boolean tempResult;
			if (rowValue == null){
				tempResult = false;
			}else{
				int lowComparison = ((Comparable)rowValue).compareTo(lowValue);
				int highComparison = ((Comparable)rowValue).compareTo(highValue);
				
				if (inclusive){
					tempResult = ((lowComparison >= 0) && (highComparison <= 0)) ;
				}else{
					tempResult = ((lowComparison > 0) && (highComparison < 0));
				}
			}
			
			if (columnGroup.isJoinTypeAnd()){
				if (result == null) result = tempResult;
				else result &= tempResult;
				if (!result) break;
			}else{
				if (result == null) result = tempResult;
				else result |= tempResult;
				if (result) break;
			}
		}
		
		return result;
	}
	

	public Object getLowValue(){
		return lowValue;
	}
	
	public void setLowValue(Object lowValue){
		this.lowValue = lowValue;
	}
	
	public Object getHighValue(){
		return highValue;
	}
	
	public void setHighValue(Object highValue){
		this.highValue = highValue;
	}
	
	public boolean getInclusive(){
		return inclusive;
	}
	
	public void setInclusive(boolean inclusive){
		this.inclusive = inclusive;
	}
}
